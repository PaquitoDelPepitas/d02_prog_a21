#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	const int TAILLE = 51;
	char adresse[TAILLE];

	cout << "Bonjour ! Veuillez saisir votre adresse s'il vous plait" << endl << "--> ";
	cin.getline(adresse, TAILLE);

	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Désolé, votre adresse est trop longue. Veuillez la ressaisir en essayant de trouver des abréviations, ou saisir une autre adresse" << endl << "--> ";
		cin.getline(adresse, TAILLE);
	}
	cout << endl;

	cout << "L'adresse saisie est " << adresse << endl;

	system("pause");
	return 0;
}