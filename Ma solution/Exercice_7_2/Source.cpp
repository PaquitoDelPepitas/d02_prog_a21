#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	double noteTravailSession, noteExamenIntra, noteExamenFinal, noteTravailSessionFINALE, noteExamenIntraFINALE, noteExamenFinalFINALE, noteFinale;
	const double TAUX_TRAVAIL_SESSION = 0.4, TAUX_EXAMEN_INTRA = 0.25, TAUX_EXAMEN_FINAL = 0.35;

	cout << "Veuillez renseigner les notes l'�tudiant dans l'ordre suivante : Travail de session, Examen intra, Examen final. "; 
	cin >> noteTravailSession >> noteExamenIntra >> noteExamenFinal; 

	noteTravailSessionFINALE = noteTravailSession * TAUX_TRAVAIL_SESSION;
	noteExamenIntraFINALE = noteExamenIntra * TAUX_EXAMEN_INTRA;
	noteExamenFinalFINALE = noteExamenFinal * TAUX_EXAMEN_FINAL;

	noteFinale = noteTravailSessionFINALE + noteExamenIntraFINALE + noteExamenFinalFINALE;

	cout << "La note finale de l'�tudiant est " << noteFinale << "%" << endl;

	system("pause");
	return 0;
}