#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	const int TAILLE = 31;
	char nom[TAILLE], prenom[TAILLE];

	cout << "Quel est votre pr�nom ?" << endl << "--> ";
	cin.getline(prenom, TAILLE);
	cout << endl;

	cout << "Quel est votre nom ?" << endl << "--> ";
	cin.getline(nom, TAILLE);
	cout << endl;

	cout << "Bonjour " << prenom << " " << nom << " !" << endl;

	system("pause");
	return 0;
}