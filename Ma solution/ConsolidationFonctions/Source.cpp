#include <iostream>
#include <iomanip>
using namespace std;

void dessinerSapin(int hauteur);
double puissanceIntPositif(double a, int n);
double puissanceInt(double a, int n);
double arrondir(double x, int n);
bool premier(int n);

int main() {
	setlocale(LC_ALL, "");

	// EXERCICE 1
	dessinerSapin(10);

	// EXERCICE 2 
	/*
	double valeur, resultat;
	int puissance;

	cout << "Saisir une valeur a multiplier : ";
	cin >> valeur;
	cout << "Saisir un puissance eniere positive : ";
	cin >> puissance;
	while (cin.fail() || cin.peek() != '\n' || puissance < 0) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Puissance ENTIERE POSITIVE uniquement requise : ";
		cin >> puissance;
	}
	cin.ignore(512, '\n');
	resultat = puissanceIntPositif(valeur, puissance);
	cout << resultat << endl
	*/

	// EXERCICE 3 
	/*
	double valeur, resultat;
	int puissance;

	cout << "Saisir une valeur a multiplier : ";
	cin >> valeur;
	cout << "Saisir un puissance eniere positive : ";
	cin >> puissance;
	resultat = puissanceInt(valeur, puissance);
	cout << resultat << endl;
	*/

	// EXERCICE 4
	/*
	double valeur, arrondi;
	int decimal;

	cout << "Saisir une valeur a arrondir : ";
	cin >> valeur;
	cout << "Saisir le nombre de decimales souhait�es : ";
	cin >> decimal;
	arrondi = arrondir(valeur, decimal);
	cout << arrondi << endl;
	*/
	
	// EXERCICE 5 
	/*
	int valeur;
	cout << "Saisir une valeur enti�re : ";
	cin >> valeur;
	cout << (premier(valeur) ? "true" : "false") << endl;
	*/


	system("pause");
	return 0;
}

void dessinerSapin(int hauteur)
{
	int espace = hauteur - 1, etoile = hauteur - espace; 

	for (int i = 0; i < hauteur; i++) {
		for (int x = 0; x < espace; x++) {
			cout << " ";
		}
		for (int y = 0; y < etoile; y++) {
			cout << "*";
		}
		espace -= 1;
		etoile += 2;
		cout << endl;
	}
}

double puissanceIntPositif(double a, int n)
{
	if (n == 0) {
		a = 1;
	}
	else {
		double multiplicateur = a;
		for (int i = 1; i < n; i++) {
			a *= multiplicateur;
		}
	}
	return a;
}

double puissanceInt(double a, int n)
{
	if (n == 0) {
		a = 1;
	}
	else if(n > 0) {
		double multiplicateur = a;
		for (int i = 1; i < n; i++) {
			a *= multiplicateur;
		}
	}
	else {
		double denominateur = a, multiplicateur = a;
		for (int i = 1; i < n * -1; i++) {
			denominateur *= multiplicateur;
			cout << denominateur << endl;
		}
		a = 1 / denominateur;
	}
	return a;
}

double arrondir(double x, int n)
{
	double puissanceDeDix = puissanceInt(10, n);
	double multiplication = x * puissanceDeDix;
	int entier = multiplication;
	double reste = multiplication - entier;
	if (reste >= 0.5) {
		entier += 1;
	}

	x = entier / puissanceDeDix;

	return x;
}

bool premier(int n)
{
	bool reponseBool;
	if (n == 0 || n == 1) {
		reponseBool = false;
	}
	else if (n == 2) {
		reponseBool = true;
	}
	else {
		int diviseur = n - 1, modulo;
		do {
			modulo = n % diviseur;
			diviseur -= 1;
		} while (diviseur > 1 || modulo != 0);
		if (modulo == 0) {
			reponseBool = false;
		}
		else {
			reponseBool = true;
		}
	}
	return reponseBool;
}
