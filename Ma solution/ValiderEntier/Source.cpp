#include <iostream>
using namespace std;

int validerSaisieEntier(int valeurSaisie);

int main() {
	int entree;

	cout << "Saisissez un entier : ";
	cin >> entree;
	entree = validerSaisieEntier(entree);

	cout << "Votre entier : " << entree << endl;

	system("pause");
	return 0;
}

int validerSaisieEntier(int valeurSaisie)
{
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un enter : ";
		cin >> valeurSaisie;
	}
	cin.ignore(512, '\n');
	return valeurSaisie;
}
