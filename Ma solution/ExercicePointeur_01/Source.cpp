#include <iostream>
using namespace std;

int main() {
	int val = 5; // adresse de la variable val : 06FF9E52
	int* pVal = &val; // adresse de la variable pVal : 06FF5C34
	int& rVal = val; // adresse de la variable rVal : 06FF9E52

	/*
	1.
	Le code  suivant affichera 5. 
	On demande ici a afficher la valeur contenu dans la variable val
	*/
	cout << val << endl;

	/*
	2.
	Le code suivant affichera 06FF9E52
	Le & indique que l'on veut afficher l'adresse de la variable val
	*/
	cout << &val << endl; 

	/*
	3.
	Le code suivant ne fonctionnera pas 
	L'ast�risque ne peut etre utilis� qu'avec des variables de type pointeur, ce qui n'est pas le cas de la variable val
	*/
	//cout << *val << endl;

	/*
	4.
	Le code suivant affichera 06FF9E52
	La variable pVal contient la valeur de l'adresse de la variable val, et on demande ici de l'afficher
	*/
	cout << pVal << endl;

	/*
	5.
	Le code suivant affichera 06FF5C34
	On veut ici afficher l'adresse de la variable pVal
	*/
	cout << &pVal << endl;

	/*
	6. 
	Le code suivant affichera 5
	On veut afficher la valeur contenu dans la variable point� par la variable pVal de type pointeur, c'est a dire la valeur de la variable dont l'adresse correspond a &val, c'est a dire la variable de val
	*/
	cout << *pVal << endl;

	/*
	7. 
	Le code suivant affichera 5 
	On veut afficher la valeur contenu dans la variable dont le pointeur rVal a recuperer la reference
	*/
	cout << rVal << endl;

	/*
	8.
	Ce code ne fonctionnera pas. 
	rVal ne se comporte pas comme une variable de type mais comme une variable de type int, on ne peut donc pas utiliser l'asterisque
	*/
	// cout << *rVal << endl;

	/*
	9.
	Ce code affichera 06FF9E52
	Il s'agit de la meme adresse que la variable val
	*/
	cout << &rVal << endl;

	/*
	10. 
	Selon moi, les variables val et rVal sont identique, car rVal ne fait que copier la variable qui correspond a l'adresse de la variable val, soit la variable va elle meme
	*/

	system("pause");
	return 0;
}