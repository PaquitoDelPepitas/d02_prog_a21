#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	double montantFacture, montantPourboire; 
	const double POURBOIRE = 0.15, FACTURE_MIN = 1;

	cout << "Quel est le montant de la facture ? ";
	cin >> montantFacture; 

	if (montantFacture >= FACTURE_MIN) {
		montantPourboire = montantFacture * POURBOIRE;
	}

	cout << "Le montant du pourboire s'�l�ve a " << montantPourboire << "$" << endl;

	system("pause");
	return 0;
}