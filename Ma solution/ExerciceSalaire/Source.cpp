#include <iostream>
#include <conio.h>
#include <ctype.h>

using namespace std;

double calculTotalCommission(double montantVentes);
double calculSalaireBase(int nbHeuresTravaille, double tauxHoraire);
double calculImpotSalaire(double salaireDeBase);
double calculImpotCommission(double commissions);
void afficherInfoEmploye(double salaireBrut, double montantTotalImposition, double salaireNet);
void afficherRecapitulatif(double totalSalaireBrut, double totalImposition, double totalSalaireNet);
char oui_non();

int checkInt();
int checkIntPlusGrandQueZero();

double checkDouble();
double checkDoublePlusGrandQueZero();
double checkDoublePositif();

int main() {
	setlocale(LC_ALL, "");

	int nbHeuresTravaillees;
	double tauxHoraire, montantVentes, salaireDeBase, salaireNet, commissions, montantImpositionSalaireBase, montantImpositionCommission, montantTotalImposition, totalSalaireBrut, totalImposition, totalSalaireNet;
	char reponse;

	totalSalaireBrut = totalImposition = totalSalaireNet = 0;

	do {
		cout << "Veuillez saisir pour cet employ� : " << endl;
		cout << "Le nombre d'heures qu'il a travaill�es --> ";
		nbHeuresTravaillees = checkIntPlusGrandQueZero();

		cout << "Son taux horaire --> ";
		tauxHoraire = checkDoublePlusGrandQueZero();

		cout << "Le montant des ventes qu'il a r�alis�es --> ";
		montantVentes = checkDoublePositif();

		commissions = calculTotalCommission(montantVentes);
		salaireDeBase = calculSalaireBase(nbHeuresTravaillees, tauxHoraire);

		montantImpositionSalaireBase = calculImpotSalaire(salaireDeBase);
		montantImpositionCommission = calculImpotCommission(commissions);
		montantTotalImposition = montantImpositionSalaireBase + montantImpositionCommission;

		salaireNet = salaireDeBase + commissions - montantTotalImposition;

		afficherInfoEmploye(salaireDeBase + commissions, montantTotalImposition, salaireNet);

		totalSalaireBrut += salaireDeBase + commissions;
		totalImposition += montantTotalImposition;
		totalSalaireNet += salaireNet;

	} while (oui_non() == 'O');

	afficherRecapitulatif(totalSalaireBrut, totalImposition, totalSalaireNet);

	system("pause");
	return 0;
}

/*
	T�che : Calculer le total de la commission obtenu en fonction du montant des ventes de l'employ�
	Param�tres: montantVentes = le montant total des ventes d'un vendeur
*/
double calculTotalCommission(double montantVentes) {
	double commissions;
	if (montantVentes < 1000) {
		commissions = montantVentes * 0.05;
	}
	else if (montantVentes <= 2000) {
		commissions = montantVentes * 0.06;
	}
	else {
		commissions = montantVentes * 0.07;
	}
	return commissions;
}

/*
	T�che : Calculer le salaire de base de l'employ�
	Param�tres: nbHeuresTravaille = nombre d'heure travaill� par l'employ�
				tauxHoraire = le montant en argent gagn� par heure
*/
double calculSalaireBase(int nbHeuresTravaille, double tauxHoraire) {
	double salaireBase = nbHeuresTravaille * tauxHoraire;
	return salaireBase;
}

/*
	T�che : Calculer l'impot sur le salaire de base
	Param�tres: salaireDeBase = le salaire de base de l'employ� sans commissions

*/
double calculImpotSalaire(double salaireDeBase) {
	double impotSalaire = salaireDeBase * (salaireDeBase < 300 ? 0.105 : 0.125);
	return impotSalaire;
}

/*
	T�che : Calculer l'impot sur le total des commissions remises � l'employ�
	Param�tres: commissions = le montant total des commissions de l'employ�

*/
double calculImpotCommission(double commissions) {
	double impotCommission = commissions * (commissions < 250 ? 0.15 : 0.175);
	return impotCommission;
}

/*
	T�che : Afficher les informations de l'employ�
	Param�tre : salaireBrut = le salaire de l'employ� incluant les commissions
				montantTotalImposition = montant total des imp�ts sur le salaire et les commissions
				salaireNet = le salaire total de l'employ� apr�s imp�t retir�
*/
void afficherInfoEmploye(double salaireBrut, double montantTotalImposition, double salaireNet) {
	cout << "Salaire Brut : " << salaireBrut << "$" << endl;
	cout << "Imposition : " << montantTotalImposition << "$" << endl;
	cout << "Salaire Net : " << salaireNet << "$" << endl << endl;
}

/*
	T�che : Afficher les informations de tous les employ�s saisis
	Param�tre : totalSalaireBrut = le cummulatif des salaires des l'employ�s incluant leurs commissions
				totalImposition = le cummulatif des imp�ts
				salaireNet = le cumulatif des salaires nets
*/
void afficherRecapitulatif(double totalSalaireBrut, double totalImposition, double totalSalaireNet) {
	cout << "Total des salaires bruts de tous les employ�s : " << totalSalaireBrut << "$" << endl;
	cout << "Total d'imposition de tous les employ�s : " << totalImposition << "$" << endl;
	cout << "Total des salaires nets de tous les employ�s : " << totalSalaireNet << "$" << endl;

}

/*
	T�che: Retourner un caract�re lu au clavier en n'acceptant
	que 'O', 'N', 'o' ou 'n'.
*/
char oui_non()
{
	char caractere;

	do
	{
		cout << "Voulez-vous renseigner les informations d'un autre employ� ?" << endl;
		cout << "(O)ui // (N)on : ";
		caractere = toupper(_getch());
		cout << endl << endl;
	} while ((caractere != 'O') && (caractere != 'N'));

	return caractere;
}

int checkInt()
{
	int valeur;
	cin >> valeur;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un nombre entier : ";
		cin >> valeur;
	}
	cin.ignore(512, '\n');
	return valeur;
}

int checkIntPlusGrandQueZero()
{
	int valeurChecked = checkInt();
	while (valeurChecked <= 0) {
		cout << "Attention - Veuillez saisir une valeur plus grande que 0 : ";
		valeurChecked = checkInt();
	}
	return valeurChecked;
}

double checkDouble()
{
	double valeur;
	cin >> valeur;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez entrer une valeur valide : ";
		cin >> valeur;
	}
	cin.ignore(512, '\n');
	return valeur;
}

double checkDoublePlusGrandQueZero()
{
	double valeurChecked = checkDouble();
	while (valeurChecked <= 0) {
		cout << "Attention - Valeur supp�rieure a 0 requise : ";
		valeurChecked = checkDouble();
	}
	return valeurChecked;
}

double checkDoublePositif()
{
	double valeurChecked = checkDouble();
	while (valeurChecked < 0) {
		cout << "Attention - Valeur d'au moins 0 requise : ";
		valeurChecked = checkDouble();
	}
	return valeurChecked;
}
