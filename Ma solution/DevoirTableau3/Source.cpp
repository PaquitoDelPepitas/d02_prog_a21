#include <iostream>
using namespace std;

int main() {

	// 1
	int tabEntiers[12] = { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34 };
	cout << "EXERCICE 1" << endl;
	for (int i = 0; i < 12; i++) {
		cout << i << " --> " << tabEntiers[i] << endl;
	}
	cout << endl;


	// 2 
	char direction[4] = { 'N', 'S' , 'E', 'O' };
	cout << "EXERCICE 2" << endl;
	for (int i = 0; i < 4; i++) {
		cout << i << " --> " << direction[i] << endl;
	}
	cout << endl;


	// 3
	const double VALEURS_CONSTANTES[6] = { 0.005,  -0.032,  0.000001,  0.167,  -100000.3,  0.015 };
	cout << "EXERCICE 3" << endl;
	for (int i = 0; i < 6; i++) {
		cout << i << " --> " << VALEURS_CONSTANTES[i] << endl;
	}
	cout << endl;


	// 4
	int tableauEntiers[10];
	cout << "EXERCICE 4" << endl;
	for (int i = 0; i < 10; i++) {
		cout << "Valeur de " << i << " --> ";
		cin >> tableauEntiers[i];
	}
	for (int n = 0; n < 10; n++) {
		cout << n << " --> " << tableauEntiers[n] << endl;
	}
	cout << endl;

	system ("pause");
	return 0;
}