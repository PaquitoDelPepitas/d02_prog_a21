#include <iostream>
using namespace std;

long long validerLongLong();
long long validerLongLongPlusGrandQue(int min);

int main() {
	setlocale(LC_ALL, "");

	const int MIN = 50;
	cout << "Entrez une valeur plus grande que " << MIN << " --> ";
	long long valeur = validerLongLongPlusGrandQue(MIN);
	cout << valeur << " est valid� !" << endl;

	system("pause");
	return 0;
}

long long validerLongLong() {
	long long aValider;
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide --> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
	return aValider;
}

long long validerLongLongPlusGrandQue(int min)
{
	long long aValider = validerLongLong();
	while (!(aValider > min)) {
		cout << "ERREUR - Veuillez saisir une valeur strictement supp�rieure a " << min << " !" << endl;
		cout << "--> ";
		aValider = validerLongLong();
	}
	return aValider;
}