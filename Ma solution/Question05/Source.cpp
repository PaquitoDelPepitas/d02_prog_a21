#include <iostream>
using namespace std;

void afficherConversionNote(double note, double ponderationInit, double ponderationNew);

int main() {
	setlocale(LC_ALL, "");

	afficherConversionNote(20, 40, 100);

	system("pause");
	return 0;
}

void afficherConversionNote(double note, double ponderationInit, double ponderationNew)
{
	cout << "La note " << note << "/" << ponderationInit << " convertie a une pondération sur " << ponderationNew << " donne le résultat suivant : ";

	double noteNew = (ponderationNew * note) / ponderationInit;

	cout << noteNew << "/" << ponderationNew << endl;
}
