#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int nombreVentes, commssionTotale;
	double recetteVentes, bonus, salaireFinal;

	const int BASE_SALAIRE = 400, COMMISSION = 200;
	const float TAUX_BONUS = 0.05;

	cout << "Veuillez renseigner le nombre de ventes r�alis�es par le vendeur, puis le montant de la recette : ";
	cin >> nombreVentes >> recetteVentes; 

	commssionTotale = nombreVentes * COMMISSION;
	bonus = recetteVentes * TAUX_BONUS;
	salaireFinal = BASE_SALAIRE + bonus + commssionTotale;

	cout << "Le salaire de l'employ� est de " << salaireFinal << "$" << endl;

	system("pause");
	return 0;
}