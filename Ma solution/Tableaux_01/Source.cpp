#include <iostream>
using namespace std;

unsigned int saisirEntier(unsigned int MIN, unsigned int MAX);
void validerUnsignedInt(unsigned int &aValider);
void validerUnsignedIntEntreBornes(unsigned int MIN, unsigned int MAX, unsigned int &aValider);

void stockerValeurs(int tab[], unsigned int nbValeurs, int &petit, int &grand);
int validerInt();

void savePlusPetit(int &petit, int nouveau);
void savePlusGrand(int &grand, int nouveau);

void afficher(int tab[], unsigned int taille, int petit, int grand);

int main() {
	setlocale(LC_ALL, "");

	const unsigned int MIN = 0, MAX = 100;
	unsigned int nbValeurs = saisirEntier(MIN, MAX);

	int tableauDeInt[100];
	int plusPetit = false, plusGrand = false;
	stockerValeurs(tableauDeInt, nbValeurs, plusPetit, plusGrand);

	afficher(tableauDeInt, nbValeurs, plusPetit, plusGrand);

	system("pause");
	return 0;
}

unsigned int saisirEntier(unsigned int MIN, unsigned int MAX)
{
	cout << "Saisissez un entier entre " << MIN << " exclus et " << MAX << " inclus --> ";
	unsigned int saisi;
	validerUnsignedIntEntreBornes(MIN, MAX, saisi);
	return saisi;
}

void validerUnsignedInt(unsigned int &aValider)
{
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide --> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerUnsignedIntEntreBornes(unsigned int MIN, unsigned int MAX, unsigned int & aValider)
{
	validerUnsignedInt(aValider);
	while (!(aValider > 0 && aValider <= 100)) {
		cout << "ERREUR - Veuillez saisir un entier compris entre " << MIN << " exclus et " << MAX << " inclus --> ";
		validerUnsignedInt(aValider);
	}
}

void stockerValeurs(int tab[], unsigned int nbValeurs, int &petit, int &grand)
{
	for (int i = 0; i < nbValeurs; i++) {
		cout << "Saisir le " << i + 1 << "e entier a stocker dans le tableau --> ";
		tab[i] = validerInt();
		savePlusPetit(petit, tab[i]);
		savePlusGrand(grand, tab[i]);
	}
}

int validerInt()
{
	int aValider;
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Valeur enti�re demand�e --> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
	return aValider;
}

void savePlusPetit(int & petit, int nouveau)
{
	if (!petit || nouveau < petit) {
		petit = nouveau;
	}
}

void savePlusGrand(int & grand, int nouveau)
{
	if (!grand || nouveau > grand) {
		grand = nouveau;
	}
}

void afficher(int tab[], unsigned int taille, int petit, int grand)
{
	cout << "Vous avez saisi les valeurs suivantes : " << endl;
	for (int i = 0; i < taille; i++) {
		cout << tab[i] << (i == (taille - 1) ? "" : " ~ ");
	}
	cout << endl;

	cout << "La plus petite valeur saisie est " << petit << endl;
	cout << "La plus grande valeur saisie est " << grand << endl;
}
