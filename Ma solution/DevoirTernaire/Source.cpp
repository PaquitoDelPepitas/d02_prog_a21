#include <iostream>
using namespace std;

int main() {
	int age;
	double rabaisApplique;
	const double PRIX = 5.00;
	const float RABAIS_JEUNE = 0.10;
	const float SEUIL_RABAIS_JEUNE = 11;
	cout << "Quel est votre age? ";
	cin >> age;

	/*
	if (age <= SEUIL_RABAIS_JEUNE) {
		rabaisApplique = (PRIX * RABAIS_JEUNE);
	}
	else {
		rabaisApplique = 0;
	}
	*/

	age <= SEUIL_RABAIS_JEUNE ? rabaisApplique = (PRIX * RABAIS_JEUNE) : rabaisApplique = 0;

	cout << "Prix final : " << PRIX - rabaisApplique << "$" << endl;
	system("pause");
	return 0;
}