#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int valeurSaisie, diviseur = 1; 
	
	cout << "Veuillez saisir une valeur enti�re : ";
	cin >> valeurSaisie;
	while (cin.fail() || cin.peek() != '\n' || valeurSaisie <= 0) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir une valeur enti�re valide : ";
		cin >> valeurSaisie;
	}

	cout << "Les diviseurs de " << valeurSaisie << " :" << endl;

	for (int i = 1; i <= valeurSaisie; i++) {
		if (valeurSaisie % diviseur == 0) {
			cout << diviseur << endl;
		}
		diviseur++;
	}

	system("pause");
	return 0;
}