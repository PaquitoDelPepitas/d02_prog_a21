#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int min, max;
	float saisie;

	cout << "Saisir une borne min : ";
	cin >> min;
	cout << "Saisir une borne max : ";
	cin >> max;
	
	cout << "Saisir une valeur : ";
	cin >> saisie; 
	while( cin.fail() || cin.peek() != '\n' || saisie <= min || saisie >= max){
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention : la valeur saisie doit �tre entre " << min << " et " << max << endl;
		cout << "Saisir une valeur : "; 
		cin >> saisie;
	}

	cout << "La valeur saisie " << saisie << " est plus grande que " << min << " et plus petite que " << max << endl;

	system("pause");
	return 0;
}