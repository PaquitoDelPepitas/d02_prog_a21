#include <iostream>
using namespace std;

int sommeMinMax(short min, short max);

int main() {
	short min, max;

	cout << "Saisir le minimum : ";
	cin >> min;
	cout << "Saisir le maximum  : ";
	cin >> max;

	cout << sommeMinMax(min, max) << endl;

	system("pause");
	return 0;
}

int sommeMinMax(short min, short max)
{
	int somme = 0;
	for (int i = min; i <= max; i++) {
		somme += i;
	}
	return somme;
}
