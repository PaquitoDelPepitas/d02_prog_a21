#include <iostream>
using namespace std;

/*
	Une �num�ration est un type
*/
enum Mois { // PascalCase
	JANVIER = 1, //MAJUSCULE_SNAKE_CASE
	FEVRIER,
	MARS,
	AVRIL,
	MAI,
	JUIN,
	JUILLET,
	AOUT,
	SEPTEMBRE,
	OCTOBRE,
	NOVEMBRE,
	DECEMBRE
};

enum TypeAvion {
	COMMERCIALE,
	PRIVE
};

/*
	une struct est un type
*/
struct date {
	unsigned short jour, annee;
	Mois mois;
};

struct avion {
	float capaciteReservoir;
	unsigned short nbPlacesAssises;
	date dateMiseEnService;
	TypeAvion type;
};

int main() {
	setlocale(LC_ALL, "");


	bool continuer;

	unsigned int totalPlacesAssises = 0;
	double sommeCapaciteReservoir = 0;
	unsigned short nbAvionsSaisis = 0;

	do
	{
		avion unAvion;
		cout << "Saisir la capacite du r�servoir : ";
		cin >> unAvion.capaciteReservoir;

		cout << "Saisir le nombre de places assises : ";
		cin >> unAvion.nbPlacesAssises;

		cout << "Saisir le jour de mise en service : ";
		cin >> unAvion.dateMiseEnService.jour;

		unsigned short moisSaisi;
		cout << "Saisir le mois de mise en service : " << endl;
		cout << "(" << Mois::JANVIER << ") JANVIER" << endl;
		cout << "(" << Mois::FEVRIER << ") FEVRIER" << endl;
		cout << "(" << Mois::MARS << ") MARS" << endl;
		cout << "(" << Mois::AVRIL << ") AVRIL" << endl;
		cout << "(" << Mois::MAI << ") MAI" << endl;
		cout << "(" << Mois::JUIN << ") JUIN" << endl;
		cout << "(" << Mois::JUILLET << ") JUILLET" << endl;
		cout << "(" << Mois::AOUT << ") AOUT" << endl;
		cout << "(" << Mois::SEPTEMBRE << ") SEPTEMBRE" << endl;
		cout << "(" << Mois::OCTOBRE << ") OCTOBRE" << endl;
		cout << "(" << Mois::NOVEMBRE << ") NOVEMBRE" << endl;
		cout << "(" << Mois::DECEMBRE << ") DECEMBRE" << endl;
		cout << "Votre choix : ";
		cin >> moisSaisi;

		unAvion.dateMiseEnService.mois = (Mois)moisSaisi;

		cout << "Saisir le annee de mise en service : ";
		cin >> unAvion.dateMiseEnService.annee;

		unsigned short typeSaisi;
		cout << "Saisir le type de l'avion :" << endl;
		cout << "(" << TypeAvion::COMMERCIALE << ") COMMERCIALE" << endl;
		cout << "(" << TypeAvion::PRIVE << ") PRIVE" << endl;
		cout << "Votre choix : ";
		cin >> typeSaisi;

		unAvion.type = (TypeAvion)typeSaisi;

		cout << "Capacite du r�servoir : " << unAvion.capaciteReservoir << endl;
		cout << "Nombre de places assises : " << unAvion.nbPlacesAssises << endl;
		cout << "Date de mise en service : " << unAvion.dateMiseEnService.jour << " ";
		if (unAvion.dateMiseEnService.mois == JANVIER) {
			cout << "JANVIER";
		}
		else if (unAvion.dateMiseEnService.mois == FEVRIER) {
			cout << "FEVRIER";
		}
		else if (unAvion.dateMiseEnService.mois == MARS) {
			cout << "MARS";
		}
		else if (unAvion.dateMiseEnService.mois == AVRIL) {
			cout << "AVRIL";
		}
		else if (unAvion.dateMiseEnService.mois == MAI) {
			cout << "MAI";
		}
		else if (unAvion.dateMiseEnService.mois == JUIN) {
			cout << "JUIN";
		}
		else if (unAvion.dateMiseEnService.mois == JUILLET) {
			cout << "JUILLET";
		}
		else if (unAvion.dateMiseEnService.mois == AOUT) {
			cout << "AOUT";
		}
		else if (unAvion.dateMiseEnService.mois == SEPTEMBRE) {
			cout << "SEPTEMBRE";
		}
		else if (unAvion.dateMiseEnService.mois == OCTOBRE) {
			cout << "OCTOBRE";
		}
		else if (unAvion.dateMiseEnService.mois == NOVEMBRE) {
			cout << "NOVEMBRE";
		}
		else if (unAvion.dateMiseEnService.mois == DECEMBRE) {
			cout << "DECEMBRE";
		}
		cout << " " << unAvion.dateMiseEnService.annee << endl;
		cout << "Type d'avion : ";
		switch (unAvion.type) {
			case 0:
				cout << "Commercial" << endl;
				break;
			case 1:
				cout << "Priv�" << endl;
				break;
			default: 
				cout << "Non pr�cis�" << endl;
				break;
		}


		totalPlacesAssises += unAvion.nbPlacesAssises;
		sommeCapaciteReservoir += unAvion.capaciteReservoir;
		nbAvionsSaisis++;

		cout << "Voulez-vous saisir un nouvel avion? (1 = oui, 0 = non) : ";
		cin >> continuer;
	} while (continuer == true);

	cout << "Total de places assises : " << totalPlacesAssises << endl;
	cout << "Moyenne des capacites de r�servoir : " << sommeCapaciteReservoir / nbAvionsSaisis << endl;


	system("pause");
	return 0;
}