#include <iostream>
using namespace std;

int main() {

	// 1 
	char nom[30];
	// Tableau qui contiendra jusqu'a 30 charactères 

	// 2
	float c[6];
	// Tableau qui contiendra jusqu'a 6 valeurs de type float

	// 3
	const int N = 50;
	int a[N];
	// Tableau qui contiendra jusqu'a N valeurs de type int, dans ce cas jusqu'a 50 valeurs

	return 0;
}