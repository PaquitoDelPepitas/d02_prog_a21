#include <iostream>
using namespace std;

void initialiserTabFloat(float tab[], const unsigned int TAILLE, float val);
void multiplierTabDouble(double tab[], const int TAILLE, float val);
float retournerPlusGrand(float tab[], const int TAILLE);

int main() {
	setlocale(LC_ALL, "");

	// 1
	const unsigned int TAILLE = 5;
	float tabFloat[TAILLE];
	initialiserTabFloat(tabFloat, TAILLE, 3.14);

	// 2
	double tabDouble[5] = { 3, 4, 5, 6, 7 };
	multiplierTabDouble(tabDouble, 5, 2.5);

	// 3
	float tabFloat2[10] = { 10.59, -846, 949, 502, 3.14, 159, 255, -648, -1, -6.548 };
	cout << "La plus grande valeur de ce tableau est " << retournerPlusGrand(tabFloat2, 10) << endl << endl;

	system("pause");
	return 0;
}

void initialiserTabFloat(float tab[], unsigned int TAILLE, float val)
{
	cout << "EXERCICE 2A" << endl;
	for (int i = 0; i < TAILLE; i++) {
		tab[i] = val;
		cout << tab[i] << endl;
	}
	cout << endl << "~*~*~*~*~*~*~" << endl << endl;;
}

void multiplierTabDouble(double tab[], const int TAILLE, float val)
{
	cout << "EXERCICE 2B" << endl;
	for (int i = 0; i < TAILLE; i++) {
		tab[i] *= val;
		cout << tab[i] << endl;
	}
	cout << endl << "~*~*~*~*~*~*~" << endl << endl;
}

float retournerPlusGrand(float tab[], const int TAILLE)
{
	cout << "EXERCICE 2C" << endl;
	float plusGrand = false;
	for (int i = 0; i < TAILLE; i++) {
		cout << tab[i] << (i == (TAILLE - 1) ? "" : " ~ ");
		if (!plusGrand || tab[i] > plusGrand) {
			plusGrand = tab[i];
		}
	}
	cout << endl;
	return plusGrand;
}
