#include <iostream>
using namespace std;

int main() {

	// 1
	float  c[8] = { 2, 5, 3, -4, 12, 12, 0, 8 };
	// Tableau contenant 8 valeurs de type float
	/* 
	c[0] = 2 
	c[1] = 5 
	c[2] = 3 
	c[3] = -4 
	c[4] = 12 
	c[5] = 12 
	c[6] = 0 
	c[7] = 8 
	*/
	cout << "EXERCICE 1" << endl;
	for (int i = 0; i < 8; i++) {
		cout << "c[" << i << "] = " << c[i] << endl;
	}
	cout << endl;


	// 2 
	float  b[8] = { 2, 5, 3, -4 };
	// Tableau qui contient 8 valeurs de types float, dont 4 sont renseignées
	/*
	b[0] = 2
	b[1] = 5
	b[2] = 3
	b[3] = -4
	b[4] = 0
	b[5] = 0
	b[6] = 0
	b[7] = 0
	*/
	cout << "EXERCICE 2" << endl;
	for (int i = 0; i < 8; i++) {
		cout << "b[" << i << "] = " << b[i] << endl;
	}
	cout << endl;


	// 3 
	int  z[12] = { 0, 0, 8, 0, 0, 6 };
	// Tableau qui contient 12 valeurs de type int, dont 6 sont renseignées
	/*
	z[0] = 0
	z[1] = 0
	z[2] = 8
	z[3] = 0
	z[4] = 0
	z[5] = 6
	z[6] = 0
	z[7] = 0
	z[8] = 0
	z[9] = 0
	z[10] = 0
	z[11] = 0
	*/
	cout << "EXERCICE 3" << endl;
	for (int i = 0; i < 12; i++) {
		cout << "z[" << i << "] = " << z[i] << endl;
	}
	cout << endl;

	system("pause");
	return 0;
}