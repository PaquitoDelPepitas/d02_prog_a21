#include <iostream>
using namespace std;

bool division(int dividende, int diviseur);

int main() {
	setlocale(LC_ALL, "");

	bool resultat = division(10, 0);
	cout << (resultat ? "true" : "false") << endl;

	system("pause");
	return 0;
}

bool division(int dividende, int diviseur)
{
	bool retour;

	if (diviseur == 0) {
		cout << "Division par " << diviseur << " impossible" << endl;
		retour = false;
	}
	else {
		double resultat = dividende / diviseur;
		cout << "La division de " << dividende << " / " << diviseur << " = " << resultat << endl;
		retour = true;
	}

	return retour;
}
