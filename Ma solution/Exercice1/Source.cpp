#include <iostream>
#include <iomanip>
using namespace std; 

int main() {
	setlocale(LC_ALL, "");

	int sommeLigue;
	double moyenneAgeEquipe, moyenneAgeLigue; 

	const int NOMBRE_EQUIPE = 5, JOUEURS_PAR_EQUIPE = 11, AGE_MIN = 18, AGE_MAX = 65;

	sommeLigue = 0;

	for (int i = 1; i <= NOMBRE_EQUIPE; i++) {

		int sommeEquipe = 0;

		for (int n = 1; n <= JOUEURS_PAR_EQUIPE; n++) {

			int age;

			do {
				cout << "Renseignez l'age du " << n << "e joueur de l'�quipe " << i << " : ";
				cin >> age;
			} while(!(age >= 18 && age <= 65));

			sommeEquipe += age;
			sommeLigue += age;
		}

		moyenneAgeEquipe = sommeEquipe / JOUEURS_PAR_EQUIPE;
		cout << "La moyenne d'age de l'�quipe " << i << " est de " << fixed << setprecision (1) << moyenneAgeEquipe << " ans." << endl << endl;
	}

	moyenneAgeLigue = sommeLigue / (JOUEURS_PAR_EQUIPE * NOMBRE_EQUIPE);
	cout << "La moyenne d'age de tous les joueurs de la ligue est de " << fixed << setprecision(1) << moyenneAgeLigue << "ans." << endl << endl;

	system("pause");
	return 0;
}