#include <iostream>
using namespace std;

const int NB_ELEMENTS = 5;

int moyenneValeursPairs(int tab[]);

int main() {
	setlocale(LC_ALL, "");

	int tab[NB_ELEMENTS] = { 1, 2, 3, 4, 5 };
	int moyenne = moyenneValeursPairs(tab);
	cout << "Moyenne = " << moyenne << endl;

	system("pause");
	return 0;
}

int moyenneValeursPairs(int tab[])
{
	int cpt = 0, somme = 0, moyenne;

	for (int i = 0; i < NB_ELEMENTS; i++) {
		if (tab[i] % 2 == 0) {
			somme += tab[i];
			cpt++;
		}
	}

	moyenne = (cpt == 0 ? 0 : somme / cpt);

	return moyenne;
}
