#include <iostream>;
using namespace std; 

int main() {
	setlocale(LC_ALL, "");

	int nombreJour;
	double consoKwh, coutKwhFinal, montantFacture;

	const double COUT_JOUR = 0.5, COUT_KWH_FORT = 0.3, COUT_KWH_REDUIT = 0.2, CONSO_KWH_MIN = 200;

	cout << "Veuillez renseigner le nombre de jour qui doivent etre pris en compte pour votre facturation : ";
	cin >> nombreJour;

	cout << "Combien de Kilowattheures avez-vous consomm�s dans ce laps de temps ? ";
	cin >> consoKwh;

	if (consoKwh > CONSO_KWH_MIN) {
		coutKwhFinal = COUT_KWH_REDUIT;
	}
	else {
		coutKwhFinal = COUT_KWH_FORT;
	}

	montantFacture = nombreJour * COUT_JOUR + consoKwh * coutKwhFinal;
	cout << "Pour votre consommation, vous serez factur� de " << montantFacture << "$" << endl;

	system("pause");
	return 0;
}