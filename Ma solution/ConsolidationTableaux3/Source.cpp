#include <iostream>
using namespace std;

void initCube(int tab[], int nbElts);
float moyenne(float tab[], int nbElts);
bool contient(int tab[], int nbElts, int n);
int occurences(int tab[], int nbElts, int n);
void afficherNegatif(float tab[], int nbElts);
int plusGrandsElements(int tab[], int nbElts, int n);
void rotationDroite(int tab[], int nbElts, int n);

int main() {

	int tab[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	rotationDroite(tab, 10, 2);

	system("pause");
	return 0;
}

void initCube(int tab[], int nbElts)
{
	for (int i = 0; i < nbElts; i++) {
		tab[i] = i * i * i;
	}
}

float moyenne(float tab[], int nbElts)
{
	float somme = 0;
	for (int i = 0; i < nbElts; i++) {
		somme += tab[i];
	}
	return somme / nbElts;
}

bool contient(int tab[], int nbElts, int n)
{
	int i = 0;
	bool test = false;
	while (i < nbElts && test == false) {
		test = (n == tab[i] ? true : false);
		i++;
	}
	return test;
}

int occurences(int tab[], int nbElts, int n)
{
	int cpt = 0;
	for (int i = 0; i < nbElts; i++) {
		cpt += (n == tab[i] ? 1 : 0);
	}
	return cpt;
}

void afficherNegatif(float tab[], int nbElts)
{
	for (int i = 0; i < nbElts; i++) {
		if (tab[i] > 0) {
			cout << i << " --> " << tab[i] << endl;
		}
	}
}

int plusGrandsElements(int tab[], int nbElts, int n)
{
	int cpt = 0;
	for (int i = 0; i < nbElts; i++) {
		cpt += (tab[i] >= n ? 1 : 0);
	}
	return cpt;
}

void rotationDroite(int tab[], int nbElts, int n)
{
	for (int k = 0; k < nbElts; k++) {
		cout << tab[k] << " ";
	}
	cout << endl;

	for (int i = n; i > 0; i--) {
		int val0 = tab[nbElts - 1];
		for (int j = nbElts - 1; j >= 0; j--) {
			tab[j] = tab[j - 1];
		}
		tab[0] = val0;
	}

	for (int k = 0; k < nbElts; k++) {
		cout << tab[k] << " ";
	}
	cout << endl;
}
