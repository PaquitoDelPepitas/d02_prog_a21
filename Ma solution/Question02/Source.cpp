#include <iostream>
using namespace std;

const unsigned short TAILLE_MAX_NOM = 40, TAILLE_MAX_PRENOM = 75;

struct etudiant {
	char nom[TAILLE_MAX_NOM];
	char prenom[TAILLE_MAX_PRENOM];
	short codeEtudiant;
	short age;
};

void validerChaineCaractere(char chaine[], unsigned short max);
void validerShort(short & aValider);
void validerShortPositif(short & aValider);

int main() {
	setlocale(LC_ALL, "");

	etudiant etudiant;

	cout << "Entrez les informations de l'�tudiant :" << endl;

	cout << "Nom : ";
	validerChaineCaractere(etudiant.nom, TAILLE_MAX_NOM);

	cout << "Pr�nom : ";
	validerChaineCaractere(etudiant.prenom, TAILLE_MAX_PRENOM);

	cout << "Code : ";
	validerShortPositif(etudiant.codeEtudiant);

	cout << "Age : ";
	validerShortPositif(etudiant.age);

	cout << endl;
	cout << "----------" << endl;
	cout << endl;

	cout << "Vous venez de saisir les informations suivantes :" << endl;
	cout << "Nom : " << etudiant.nom << endl;
	cout << "Pr�nom : " << etudiant.prenom << endl;
	cout << "Code : " << etudiant.codeEtudiant << endl;
	cout << "Age : " << etudiant.age << endl;

	cout << endl;

	system("pause");
	return 0;
}

void validerChaineCaractere(char chaine[], unsigned short max)
{
	cin.getline(chaine, max);
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Votre saisie est trop longue. Veuillez effectuer une nouvelle saisie en respectant la limite de " << max << "caract�res" << endl;
		cin.getline(chaine, max);
	}
}

void validerShort(short & aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide --> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerShortPositif(short & aValider)
{
	validerShort(aValider);
	while (!(aValider > 0)) {
		cout << "ERREUR - Veuillez saisir une valeur strictement supp�rieure a 0 !" << endl;
		cout << "--> ";
		validerShort(aValider);
	}
}
