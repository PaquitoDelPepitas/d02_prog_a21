#include <iostream>;
using namespace std;

enum Choix {
	AJOUTER = 'A',
	SOUSTRAIRE = 'S',
	MULTIPLIER = 'M',
	QUITTER = 'Q'
};

int main() {
	setlocale(LC_ALL, "");

	double resultat = 0;
	char saisie;
	const double VALEUR_AJOUTER = 1, VALEUR_SOUSTRACTION = 4, VALEUR_MULTIPLICATION = 2;
	// const char AJOUTER = 'A', SOUSTRAIRE = 'S', MULTIPLIER = 'M', QUITTER = 'Q';

	do {
		cout << endl;
		cout << "Valeur : " << resultat << endl;
		cout << "(" << (char)Choix::AJOUTER << ")jouter " << VALEUR_AJOUTER << endl;
		cout << "(" << (char)Choix::SOUSTRAIRE << ")oustraire " << VALEUR_SOUSTRACTION << endl;
		cout << "(" << (char)Choix::MULTIPLIER << ")ultiplier par " << VALEUR_MULTIPLICATION << endl;
		cout << "(" << (char)Choix::QUITTER << ")uitter " << endl;
		cout << "Votre saisie : ";

		cin >> saisie; 
		while (cin.fail() || cin.peek() != '\n' || ((int)saisie != Choix::AJOUTER && (int)saisie != Choix::SOUSTRAIRE && (int)saisie != Choix::MULTIPLIER && (int)saisie != Choix::QUITTER)) {
			cin.clear();
			cin.ignore(512, '\n');
			cout << "Erreur - Veuillez saisir une des lettres valides : ";
			cin >> saisie;
		}

		switch (saisie) {
			case (char)Choix::AJOUTER:
				resultat += VALEUR_AJOUTER;
				break;
			case (char)Choix::SOUSTRAIRE: 
				resultat -= VALEUR_SOUSTRACTION;
				break;
			case (char)Choix::MULTIPLIER:
				resultat *= VALEUR_MULTIPLICATION;
				break;
			case (char)Choix::QUITTER:
				break;
			default: 
				break;
		}

	} while (saisie != QUITTER);

	return 0;
}