#include <iostream>
#include <cmath>
using namespace std;

void entrerTab(int tab[], int taille);
void initialiserTabZ(int tabX[], int tabY[], int tabZ[], int taille);
void afficherTableaux(int tabX[], int tabY[], int tabZ[], int taille);
void racineCarree(int tab[], int taille, double &resultat);

int main() {
	setlocale(LC_ALL, "");

	const int TAILLE = 4;
	int tabX[TAILLE] = {}, tabY[TAILLE] = {}, tabZ[TAILLE];

	entrerTab(tabX, TAILLE / 2);
	entrerTab(tabY, TAILLE / 2);

	initialiserTabZ(tabX, tabY, tabZ, TAILLE);

	afficherTableaux(tabX, tabY, tabZ, TAILLE);

	double resultat;
	racineCarree(tabZ, TAILLE, resultat);
	cout << "Racine carr�e de la somme des �l�ments de Z : " << resultat << endl;

	system("pause");
	return 0;
}

void entrerTab(int tab[], int taille)
{
	for (int i = 0; i < taille; i++) {
		int valeur;
		cout << "Entrer une valeur enti�re --> ";
		cin >> valeur;
		tab[i] = valeur;
	}
}

void initialiserTabZ(int tabX[], int tabY[], int tabZ[], int taille)
{
	int index = 0;
	for (int i = 0; i < taille / 2; i++) {
		tabZ[index] = tabX[i];
		index++;
	}
	for (int j = 0; j < taille / 2; j++) {
		tabZ[index] = tabY[j];
		index++;
	}
}

void afficherTableaux(int tabX[], int tabY[], int tabZ[], int taille)
{
	cout << endl;
	for (int i = 0; i < taille; i++) {
		cout << "|   " << tabX[i] << "   |   " << tabY[i] << "   |   " << tabZ[i] << "   |" << endl;
	}
	cout << endl;
}

void racineCarree(int tab[], int taille, double &resultat)
{
	int somme = 0;
	for (int i = 0; i < taille; i++) {
		somme += tab[i];
	}
	resultat = sqrt(somme);
}
