#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	double valeurA, valeurB, valeurC;

	cout << "Veuillez �crire les trois valeurs que vous voulez comparer : " << endl;
	cin >> valeurA >> valeurB >> valeurC;

	if (valeurA > valeurB && valeurA > valeurC) {
		cout << valeurA << " est la plus grande valeur !" << endl;
	}
	else if (valeurB > valeurA && valeurB > valeurC) {
		cout << valeurB << " est la plus grande valeur !" << endl;
	}
	else {
		cout << valeurC << " est la plus grande valeur !" << endl;
	}

	system("pause");
	return 0;
}