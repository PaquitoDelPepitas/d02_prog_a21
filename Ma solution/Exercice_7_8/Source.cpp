#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	bool piece1, piece2;
	const double GAIN_MAX = 10, GAIN_PETIT = 5;

	cout << "Quel est le r�sultat de la premi�re pi�ce ? (0 = pile, 1 = face)" << endl;
	cin >> piece1; 
	cout << "Quel est le r�sultat de la seconde pi�ce ? (0 = pile, 1 = face)" << endl;
	cin >> piece2;

	if (piece1 && piece2) {
		cout << "Vous avez deux faces, vous obtenez donc " << GAIN_MAX << "$" << endl;
	}
	else if (piece1 || piece2) {
		cout << "Vous avez un pile et un face, vous obtenez donc " << GAIN_PETIT << "$" << endl;
	}
	else {
		cout << "Vous avez deux faces, vous avez perdu." << endl;
	}

	system("pause");
	return 0;
}