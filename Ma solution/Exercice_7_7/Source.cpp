#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	double nombreA, nombreB, nombreC;

	cout << "Veuillez entrer trois nombres de votre choix : " << endl;
	cin >> nombreA >> nombreB >> nombreC;

	if ((nombreA + nombreB) == nombreC) {
		cout << nombreC << " est solution !" << endl;
	}
	else if ((nombreB + nombreC) == nombreA) {
		cout << nombreA << " est solution !" << endl; 
	}
	else if ((nombreA + nombreC) == nombreB) {
		cout << nombreB << " est solution !" << endl; 
	}
	else {
		cout << "Pas de solution." << endl;
	}

	system("pause");
	return 0;
}