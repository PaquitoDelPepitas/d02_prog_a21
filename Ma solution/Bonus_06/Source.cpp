#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	const int MIN = 0, MAX = 10000;

	int saisie;
	cout << "Saisissez une valeur enti�re entre " << MIN << " et " << MAX << " : ";
	cin >> saisie;
	while (cin.fail() || cin.peek() != '\n' || saisie < 0 || saisie > MAX) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Valeur entre " << MIN << " et " << MAX << " seulement : ";
		cin >> saisie;
	}
	cin.ignore(512, '\n');

	int reste = saisie, nbMillier = 0, nbCentaine = 0, nbDizaine = 0, nbUnite = 0;

	while (reste >= 1000) {
		reste -= 1000;
		nbMillier++;
	}
	while (reste >= 100) {
		reste -= 100;
		nbCentaine++;
	}
	while (reste >= 10) {
		reste -= 10;
		nbDizaine++;
	}
	while (reste > 0) {
		reste -= 1;
		nbUnite++;
	}
	
	if (nbMillier > 0) {
		cout << nbMillier << " millier" << (nbMillier > 1 ? "s" : "") << endl;
		cout << nbCentaine << " centaine" << (nbCentaine > 1 ? "s" : "") << endl;
		cout << nbDizaine << " dizaine" << (nbDizaine > 1 ? "s" : "") << endl;
		cout << nbUnite << " unit�" << (nbUnite > 1 ? "s" : "") << endl;
	} 
	else if (nbCentaine > 0) {
		cout << nbCentaine << " centaine" << (nbCentaine > 1 ? "s" : "") << endl;
		cout << nbDizaine << " dizaine" << (nbDizaine > 1 ? "s" : "") << endl;
		cout << nbUnite << " unit�" << (nbUnite > 1 ? "s" : "") << endl;
	}
	else if (nbDizaine > 0) {
		cout << nbDizaine << " dizaine" << (nbDizaine > 1 ? "s" : "") << endl;
		cout << nbUnite << " unit�" << (nbUnite > 1 ? "s" : "") << endl;
	}
	else {
		cout << nbUnite << " unit�" << (nbUnite > 1 ? "s" : "") << endl;
	}


	system("pause");
	return 0;
}