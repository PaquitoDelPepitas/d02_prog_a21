#include <iostream>; 
using namespace std; 

int main() {
	setlocale(LC_ALL, "");

	int nombreSaisi, iteration;

	cout << "Veuillez saisir une valeur enti�re de d�part : "; 
	cin >> nombreSaisi;

	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir une valeur enti�re : ";
		cin >> nombreSaisi;
	}

	cout << "Combien de fois voulez-vous it�rer la valeur saisie pr�c�demment ? ";
	cin >> iteration;

	while (cin.fail() || cin.peek() != '\n' || iteration <= 0) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Erreur - Veuillez saisir une valeur valide : ";
		cin >> iteration;
	}

	for (int i = 1; i <= iteration; i++) {
		cout << nombreSaisi << endl;
		nombreSaisi++;
	}

	system("pause");
	return 0;
}