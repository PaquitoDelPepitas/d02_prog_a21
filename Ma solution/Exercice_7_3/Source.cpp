#include <iostream>; 
using namespace std; 

int main() {
	setlocale(LC_ALL, "");

	double nourriture, courant;
	cout << "Renseignez vos d�penses hebdomadaires : D'abbord pour la nourriture et les prpduits m�nagers, puis les d�penses courantes." << endl;
	cin >> nourriture >> courant;
	double depensesHEBDO = (nourriture + courant) * 4;

	double transport, loyer, facture;
	cout << endl << "Renseignez dans l'ordre vos d�penses mensuels pour le transport, le loyer, et les factures." << endl;
	cin >> transport >> loyer >> facture;
	double depensesMOIS = transport + loyer + facture;

	double depensesTOTAL = depensesHEBDO + depensesMOIS;

	double cheque;
	cout << "Renseignez le montant de votre ch�que de paye : ";
	cin >> cheque;
	double payeMOIS = cheque * 2;

	double difference = payeMOIS - depensesTOTAL;
	cout << "Ce mois-ci vous avez gagn� " << payeMOIS << "$ et avez d�pens� " << depensesTOTAL << "$. Il vous reste " << difference << "$ d'�conomies !" << endl;

	system("pause");
	return 0;
}