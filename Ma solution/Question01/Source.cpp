#include <iostream>
using namespace std;

double montantImpotAPayer(double salaireBrut);

int main() {
	setlocale(LC_ALL, "");

	system("pause");
	return 0;
}

double montantImpotAPayer(double salaireBrut)
{
	double impot;
	const double BORNE_MIN = 20000, BORNE_MAX = 45000;
	const double TAUX_IMPOT_1 = 0.20, TAUX_IMPOT_2 = 0.25, TAUX_IMPOT_3 = 0.30;

	if (salaireBrut < BORNE_MIN) {
		impot = TAUX_IMPOT_1;
	}
	else if (salaireBrut >= BORNE_MAX) {
		impot = TAUX_IMPOT_3;
	}
	else {
		impot = TAUX_IMPOT_2;
	}

	return impot;
}
