#include <iostream>
#include <time.h>
using namespace std;

void genererValeurAleatoire(int MIN, int MAX, int &valeurAleatoire);
void validerInt(int &saisie);
void validerIntEntreBornes(int MIN, int MAX, int &saisie);
int checkReussite(int nbMystere, int saisie);
void afficherInfo(int resultat);
void joueurJoue();
void ordiJoue();

int main() {
	setlocale(LC_ALL, "");
	srand(time(0));

	int choix;

	cout << "Choisissez le jeu auquel vous voulez jouer :" << endl;
	cout << "(1) - Deviner le nombre myst�re" << endl; 
	cout << "(2) - Choisir le nombre myst�re" << endl;
	cout << "Votre choix --> ";
	cin >> choix;

	choix == 1 ? joueurJoue() : ordiJoue();

	system("pause");
	return 0;
}

void genererValeurAleatoire(int MIN, int MAX, int &valeurAleatoire)
{
	valeurAleatoire = rand() % (MAX - MIN + 1) + MIN;
}

void validerInt(int &saisie)
{
	cin >> saisie;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide --> ";
		cin >> saisie;
	}
	cin.ignore(512, '\n');
}

void validerIntEntreBornes(int MIN, int MAX, int &saisie)
{
	validerInt(saisie);
	while (!(saisie >= MIN && saisie <= MAX)) {
		cout << "ERREUR - Veuillez saisir une valeur comprise entre " << MIN << " et " << MAX << " --> ";
		validerInt(saisie);
	}
}

int checkReussite(int nbMystere, int saisie)
{
	int resultat;
	if(saisie < nbMystere){
		resultat = -1;
	}
	else if (saisie > nbMystere) {
		resultat = 1;
	}
	else {
		resultat = 0;
	}
	return resultat;
}

void afficherInfo(int resultat)
{
	switch (resultat)
	{
		case -1:
			cout << "C'est plus !" << endl;
			break;
		case 1: 
			cout << "C'est moins !" << endl;
			break;
		default:
			cout << "C'est gagn� !" << endl;
			break;
	}
}

void joueurJoue()
{
	const int MIN = 1, MAX = 100;
	int objectif;
	genererValeurAleatoire(MIN, MAX, objectif);

	int resultat, tentative = 0;

	do {
		tentative += 1;
		int saisieJoueur;
		cout << "Saisir une valeur entre 1 et 100 --> ";
		validerIntEntreBornes(MIN, MAX, saisieJoueur);

		resultat = checkReussite(objectif, saisieJoueur);

		afficherInfo(resultat);
	} while (resultat != 0);

	int gain[10] = { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 };
	cout << "Vous aves mis " << tentative << " tours pour trouver le nombre myst�re, vous remportez donc " << (tentative > 10 ? 0 : gain[tentative - 1]) << "$" << endl;
}

void ordiJoue()
{
	const int MIN = 1, MAX = 100;

	cout << "Veuillez saisir un nombre myst�re entre 1 et 100 que l'ordinateur devra deviner --> ";
	int saisieJoueur;
	validerIntEntreBornes(MIN, MAX, saisieJoueur);

	int tentative = 0, choixOrdi, resultat;


	do {
		tentative += 1;
		genererValeurAleatoire(MIN, MAX, choixOrdi);
		cout << "L'ordinateur choisit " << choixOrdi << endl;

		resultat = checkReussite(saisieJoueur, choixOrdi);

		afficherInfo(resultat);
	} while (resultat != 0);

	int gain[10] = { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 };
	cout << "L'ordinateur a mis " << tentative << " tours pour trouver le nombre myst�re, il remporte donc " << (tentative > 10 ? 0 : gain[tentative - 1]) << "$" << endl;
}
