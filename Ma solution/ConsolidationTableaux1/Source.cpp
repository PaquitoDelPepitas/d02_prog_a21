#include <iostream>
using namespace std;

enum Categorie {
	ASSURANCES,
	DIVERS, 
	HABILLEMENT, 
	LOISIRS, 
	LOYER, 
	NOURRITURE, 
	TRANSPORT
};

void afficherMenu(int &choix);
void validerInt(int &aValider);
void validerIntEntreBorne(int min, int max, int &aValider);
void choixCategorie(int categorie, double assurance[], double divers[], double habillement[], double loisirs[], double loyer[], double nourriture[], double transport[], int &compteurAssurance, int &compteurDivers, int &compteurHabillement , int &compteurLoisirs, int &compteurLoyer, int &compteurNourriture, int &compteurTransport);
void ajouterDepense(double tab[], int &compteur);
void validerDouble(double &aValider);
void validerDoublePositif(double &aValider);
void calculerDepenses(double assurance[], double divers[], double habillement[], double loisirs[], double loyer[], double nourriture[], double transport[], int &compteurAssurance, int &compteurDivers, int &compteurHabillement, int &compteurLoisirs, int &compteurLoyer, int &compteurNourriture, int &compteurTransport);
void depensesCategorie(int categorie, double &total, double assurance[], double divers[], double habillement[], double loisirs[], double loyer[], double nourriture[], double transport[], int &compteurAssurance, int &compteurDivers, int &compteurHabillement, int &compteurLoisirs, int &compteurLoyer, int &compteurNourriture, int &compteurTransport);
void calculDepensesCategorie(double tab[], int &compteur, double &total, int i);
void afficherCategorie(int i);


int main() {
	setlocale(LC_ALL, "");

	int choix;
	double assurance[] = { 0 }, divers[] = { 0 }, habillement[] = { 0 }, loisirs[] = { 0 }, loyer[] = { 0 }, nourriture[] = { 0 }, transport[] = { 0 };
	int compteurAssurance = 0, compteurDivers = 0, compteurHabillement = 0, compteurLoisirs = 0, compteurLoyer = 0, compteurNourriture = 0, compteurTransport = 0;

	do {
		afficherMenu(choix);
		if (choix != -1) {
			choixCategorie(choix, assurance, divers, habillement, loisirs, loyer, nourriture, transport, compteurAssurance, compteurDivers, compteurHabillement, compteurLoisirs, compteurLoyer, compteurNourriture, compteurTransport);
		}
	} while (choix != -1);

	calculerDepenses(assurance, divers, habillement, loisirs, loyer, nourriture, transport, compteurAssurance, compteurDivers, compteurHabillement, compteurLoisirs, compteurLoyer, compteurNourriture, compteurTransport);


	system("pause");
	return 0;
}

void afficherMenu(int &choix)
{
	cout << "Quelle d�pense voulez-vous renseigner ?" << endl;
	cout << "(" << Categorie::ASSURANCES << ") Assurances" << endl;
	cout << "(" << Categorie::DIVERS << ") Divers" << endl;
	cout << "(" << Categorie::HABILLEMENT << ") Habillement" << endl;
	cout << "(" << Categorie::LOISIRS << ") Loisirs" << endl;
	cout << "(" << Categorie::LOYER << ") Loyer" << endl;
	cout << "(" << Categorie::NOURRITURE << ") Nourriture" << endl;
	cout << "(" << Categorie::TRANSPORT << ") Transport" << endl;
	cout << "(-1) Arreter et obtenir le r�sum�" << endl; 
	cout << "--> ";

	validerIntEntreBorne(-1, 6, choix);
}

void validerInt(int &aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide" << endl << "--> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerIntEntreBorne(int min, int max, int & aValider)
{
	validerInt(aValider);
	while (!(aValider >= min && aValider <= max)) {
		cout << "ERREUR - Veuillez saisir une valeur entre " << min << " et " << max << " inclus" << endl << "--> ";
		validerInt(aValider);
	}
}

void choixCategorie(int categorie, double assurance[], double divers[], double habillement[], double loisirs[], double loyer[], double nourriture[], double transport[], int &compteurAssurance, int &compteurDivers, int &compteurHabillement, int &compteurLoisirs, int &compteurLoyer, int &compteurNourriture, int &compteurTransport)
{
	switch (categorie) {
		case Categorie::ASSURANCES:
			ajouterDepense(assurance, compteurAssurance);
			break;
		case Categorie::DIVERS:
			ajouterDepense(divers, compteurDivers);
			break;
		case Categorie::HABILLEMENT:
			ajouterDepense(habillement, compteurHabillement);
			break;
		case Categorie::LOISIRS:
			ajouterDepense(loisirs, compteurLoisirs);
			break;
		case Categorie::LOYER:
			ajouterDepense(loyer, compteurLoyer);
			break;
		case Categorie::NOURRITURE:
			ajouterDepense(nourriture, compteurNourriture);
			break;
		case Categorie::TRANSPORT:
			ajouterDepense(transport, compteurTransport);
			break;
	}
}

void ajouterDepense(double tab[], int & compteur)
{
	cout << "Quel est le montant de votre d�pense dans cette cat�gorie ?" << endl << "--> ";
	double montant;
	validerDoublePositif(montant);
	tab[compteur] = montant;
	compteur += 1;
}

void validerDouble(double &aValider)
{
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide" << endl << "--> ";
		cin >> aValider;
	}
}

void validerDoublePositif(double & aValider)
{
	validerDouble(aValider);
	while (!(aValider > 0)) {
		cout << "ERREUR - Une d�pense ne peut etre de 0 ou moins. Veuillez saisir un montant valide" << endl << "--> ";
		validerDouble(aValider);
	}
}

void calculerDepenses(double assurance[], double divers[], double habillement[], double loisirs[], double loyer[], double nourriture[], double transport[], int &compteurAssurance, int &compteurDivers, int &compteurHabillement, int &compteurLoisirs, int &compteurLoyer, int &compteurNourriture, int &compteurTransport) {
	double depensesTotales = 0;
	for (int i = 0; i <= 6; i++) {
		depensesCategorie(i, depensesTotales, assurance, divers, habillement, loisirs, loyer, nourriture, transport, compteurAssurance, compteurDivers, compteurHabillement, compteurLoisirs, compteurLoyer, compteurNourriture, compteurTransport);
	}
	cout << "Total des d�penses : " << depensesTotales << "$" << endl;
}

void depensesCategorie(int categorie, double & total, double assurance[], double divers[], double habillement[], double loisirs[], double loyer[], double nourriture[], double transport[], int &compteurAssurance, int &compteurDivers, int &compteurHabillement, int &compteurLoisirs, int &compteurLoyer, int &compteurNourriture, int &compteurTransport)
{
	switch (categorie) {
		case Categorie::ASSURANCES:
			calculDepensesCategorie(assurance, compteurAssurance, total, categorie);
			break;
		case Categorie::DIVERS:
			calculDepensesCategorie(divers, compteurDivers, total, categorie);
			break;
		case Categorie::HABILLEMENT:
			calculDepensesCategorie(habillement, compteurHabillement , total, categorie);
			break;
		case Categorie::LOISIRS:
			calculDepensesCategorie(loisirs, compteurLoisirs, total, categorie);
			break;
		case Categorie::LOYER:
			calculDepensesCategorie(loyer, compteurLoyer, total, categorie);
			break;
		case Categorie::NOURRITURE:
			calculDepensesCategorie(nourriture, compteurNourriture, total, categorie);
			break;
		case Categorie::TRANSPORT:
			calculDepensesCategorie(transport, compteurTransport, total, categorie);
			break;
	}
}

void calculDepensesCategorie(double tab[], int & compteur, double & total, int i)
{
	double totalCategorie = 0;
	for (int x = 0; x < compteur; x++) {
		totalCategorie += tab[x];
	}
	cout << "Total des d�penses ";
	afficherCategorie(i);
	cout << " : " << totalCategorie << "$" << endl;
	total += totalCategorie;
}

void afficherCategorie(int i)
{
	switch (i) {
	case Categorie::ASSURANCES:
		cout << "Assurances";
		break;
	case Categorie::DIVERS:
		cout << "Divers";
		break;
	case Categorie::HABILLEMENT:
		cout << "Habillement";
		break;
	case Categorie::LOISIRS:
		cout << "Loisirs";
		break;
	case Categorie::LOYER:
		cout << "Loyer";
		break;
	case Categorie::NOURRITURE:
		cout << "Nourriture";
		break;
	case Categorie::TRANSPORT:
		cout << "Transport";
		break;
	}
}
