#include <iostream>
#include <iomanip>
#include <conio.h>
using namespace std;

const unsigned short TAILLE_NOM = 31;
const unsigned short TAILLE_PRENOM = 31;

const unsigned int NB_MAX_COMPTES = 1000;
const unsigned int NO_COMPTE_MIN = 0;
const unsigned int NO_COMPTE_MAX = NB_MAX_COMPTES - 1;

struct compte {
	unsigned int noCompte;
	char nom[TAILLE_NOM];
	char prenom[TAILLE_PRENOM];
	double solde;
	bool actif;
};

enum TypeTransaction {
	DEPOT,
	RETRAIT
};

void afficherMenu(compte comptes[], unsigned int nbComptes);

void ajouterCompte(compte comptes[], unsigned int& nbComptesActuels);
void desactiverCompte(compte comptes[], unsigned int indice);

double calculerSommeSoldes(compte comptes[], unsigned int nbComptes);

unsigned int recupererNombreCompteActif(compte comptes[], unsigned int nbComptes);

void afficherSoldeMoyen(compte comptes[], unsigned int nbComptes);
void afficherSommeSoldes(compte comptes[], unsigned int nbComptes);
int rechercherClient(compte comptes[], unsigned int nbComptes);
void afficherTousLesComptes(compte comptes[], unsigned int nbComptes);
void afficherCompte(compte compte);

void effectuerTransaction(TypeTransaction typeTransaction, compte &compte, double montant);
void effectuerDepot(compte &compte, double montant);
void effectuerRetrait(compte &compte, double montant);

void validerUnsignedInt(unsigned int &aValider);
void validerUnsignedIntEntreBornes(unsigned int &aValider, int MIN, int MAX);

void entrerPrenom(compte comptes[], unsigned int nbCompteActuel, const int TAILLE_PRENOM);
void entrerNom(compte comptes[], unsigned int nbCompteActuel, const int TAILLE_NOM);

void validerDouble(double &aValider);
void validerDoublePositif(double &aValider);
void validerDoublePlusGrandQueZero(double &aValider);

void preparationDepot(compte comptes[], unsigned int noCompte);
void preparationRetrait(compte comptes[], unsigned int noCompte);
void verification(compte comptes[], unsigned int nbComptes, unsigned int saisie);
void verificationBis(compte comptes[], unsigned int nbComptes, unsigned int saisie);

void choixMontant(double &montant, TypeTransaction transaction);

double calculerSoldeMoyen(compte comptes[], unsigned int nbComptes);

void signature();

int main() {
	setlocale(LC_ALL, "");

	compte comptes[NB_MAX_COMPTES];
	unsigned int nbComptes = 0;
	
	afficherMenu(comptes, nbComptes);

	return 0;
}

/*
	T�che : afficher le menu principal
	param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes dans l'application en ce moment
*/
void afficherMenu(compte comptes[], unsigned int nbComptes) {

	unsigned int saisie;

	do {
		cout << "~*~ BIENVENUE CHEZ JARDIBANK ~*~" << endl << endl;
		cout << "Que souhaitez-vous faire ?" << endl;
		cout << "(1) - Ajouter un compte bancaire" << endl;
		cout << "(2) - D�sactiver un compte bancaire" << endl;
		cout << "(3) - Obtenir les informations d'un compte" << endl;
		cout << "(4) - Effectuer un d�pot" << endl;
		cout << "(5) - Effectuer un retrait" << endl;
		cout << "(6) - Afficher la liste de tous nos clients" << endl;
		cout << "(7) - Obtenir le solde de tous les comptes Jardibank" << endl;
		cout << "(8) - Obtenir le solde moyen des comptes" << endl;
		cout << "-----" << endl;
		cout << "(9) - Quitter l'application" << endl << endl;
		cout << "--> ";

		const int MIN_MENU = 1, MAX_MENU = 9;
		validerUnsignedIntEntreBornes(saisie, MIN_MENU, MAX_MENU);

		switch (saisie) {
			case 1:
				ajouterCompte(comptes, nbComptes);
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
				verification(comptes, nbComptes, saisie);
				break;
			default:
				break;
		}

	} while (saisie != 9);
}


/*
	T�che : ajouter un compte
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes dans l'application en ce moment

*/
void ajouterCompte(compte comptes[], unsigned int& nbComptesActuels) {
	if (nbComptesActuels != NB_MAX_COMPTES) {
		cout << endl;
		cout << "Vous avez choisi de cr�er un compte chez Jardibank." << endl;

		comptes[nbComptesActuels].noCompte = nbComptesActuels;
		cout << "Le num�ro attribu� a votre compte est le " << comptes[nbComptesActuels].noCompte << endl;
		cout << endl;

		cout << "Nous avons maintenant besoin de vos informations personnelles." << endl;
		cout << "Veuillez renseigner votre pr�nom --> ";
		entrerPrenom(comptes, nbComptesActuels, TAILLE_PRENOM);
		cout << "Veuillez renseigner votre nom de famille --> ";
		entrerNom(comptes, nbComptesActuels, TAILLE_NOM);
		cout << endl;

		cout << "La cr�ation de votre compte est bientot termin�e !" << endl;
		cout << "Quel montant initiale voulez-vous placer dans votre compte Jardibank ?" << endl << "--> ";
		validerDoublePositif(comptes[nbComptesActuels].solde);
		cout << endl;

		comptes[nbComptesActuels].actif = true;
		nbComptesActuels += 1;
		cout << "F�licitations, votre compte Jardibank a �t� cr�� avec succ�s ! Il est actif d�s maintenant.";
		signature();
	}
	else {
		cout << endl;
		cout << "Malheureusement, nous ne pouvons plus accepter de nouveau client en raison de la crise �conomique... " << endl;
		cout << "Jardibank tient a vous exprimer ses plus plates excuses.";
		cout << endl;
		signature();
	}
}

/*
	T�che : d�sactiver un compte si son solde est 0, sinon afficher un message
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes dans l'application en ce moment
*/
void desactiverCompte(compte comptes[], unsigned int indice) {
	cout << endl;
	if (comptes[indice].solde > 0) {
		cout << "La d�sactivation du compte num�ro " << comptes[indice].noCompte << " ne peut aboutir car son solde est de " << comptes[indice].solde << "$" << endl;
		cout << "Merci de vider enti�rement le solde de ce compte, nous pourrons ensuite proc�der a sa d�sactivation." << endl;
	}
	else {
		comptes[indice].actif = false;
		cout << comptes[indice].prenom << " " << comptes[indice].nom << ", votre compte est d�sormais d�sactiv�, vous ne pourrez plus r�aliser d'op�ration." << endl; 
		cout << "Pour r�activer votre compte JardiBank, veuillez nous envoyer une lettre recommand�e en y mentionnant de mani�re explicite cette requete." << endl;
	}
	signature();
}

/*
	T�che : calculer la somme de toutes les comptes actifs
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes saisies dans l'application en ce moment
*/
double calculerSommeSoldes(compte comptes[], unsigned int nbComptes) {
	double somme = 0;
	for (int i = 0; i < nbComptes; i++) {
		if (comptes[i].actif == true) {
			somme += comptes[i].solde;
		}
	}
	return somme;
}

/*
	T�che : afficher la somme de toutes les comptes actifs
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes saisies dans l'application en ce moment
*/
void afficherSommeSoldes(compte comptes[], unsigned int nbComptes) {
	double somme = calculerSommeSoldes(comptes, nbComptes);
	cout << endl;
	cout << "La somme de tous les soldes des comptes Jardibank actifs est de " << fixed << setprecision(2) << somme << "$" << endl;
	signature();
}

/*
	T�che : r�cup�rer le nombre total de comptes actifs
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes saisies dans l'application en ce moment
*/
unsigned int recupererNombreCompteActif(compte comptes[], unsigned int nbComptes) {
	unsigned int cpt = 0;
	for (int i = 0; i < nbComptes; i++) {
		cpt += (comptes[i].actif == true ? 1 : 0);
	}
	return cpt;
}

/*
	T�che : affcher la moyenne des soldes de toutes les comptes actifs
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes saisies dans l'application en ce moment
*/
void afficherSoldeMoyen(compte comptes[], unsigned int nbComptes) {
	double soldeMoyen = calculerSoldeMoyen(comptes, nbComptes);
	cout << endl;
	cout << "La solde moyen de tous les soldes des comptes Jardibank actifs est de " << fixed << setprecision(2) << soldeMoyen << "$" << endl;
	signature();
}



/*
	T�che : demander un num�ro de compte et rechercher un compte actif en fonction du num�ro de client saisie. Affiche "Compte inexistant" et retourne -1 si le compte
	est inexistant, sinon retourne l'indice dans le tableau ou se trouve le compte.
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes saisies dans l'application en ce moment
*/
int rechercherClient(compte comptes[], unsigned int nbComptes) {
	cout << endl;
	cout << "Pour v�rifier votre identit�, veuillez saisir votre num�ro de compte." << endl << "--> ";
	unsigned int choixCompte;
	int retour;
	validerUnsignedIntEntreBornes(choixCompte, NO_COMPTE_MIN, NO_COMPTE_MAX);
	if(choixCompte > nbComptes - 1){
		cout << endl;
		cout << "Compte inexistant" << endl;
		retour = -1;
		signature();
	}
	else if (comptes[choixCompte].actif == false) {
		cout << endl;
		cout << "Desol�e, le compte Jardibank portant le num�ro " << choixCompte << " est d�sactiv�." << endl;
		cout << "S'il vous appartient et que vous voulez le r�activer, nous vous demandons de nous faire parvenir une lettre recommand�e en mentionnant explicitement cette requete." << endl;
		retour = -1;
		signature();
	}
	else {
		retour = choixCompte;
	}
	return retour;
}

/*
	T�che : affiche les informations de toutes les comptes actifs.
	Param�tres : comptes : repr�sente la liste compl�te des comptes dans l'application
				 nbComptes : repr�sente le nombre de comptes saisies dans l'application en ce moment
*/
void afficherTousLesComptes(compte comptes[], unsigned int nbComptes) {
	for (int i = 0; i < nbComptes; i++) {
		if (comptes[i].actif == true) {
			afficherCompte(comptes[i]);
		}
	}
	cout << endl;
	signature();
}

/*
	T�che : affiche les informations d'un seul compte.
	Param�tres : compte : repr�sente le compte d'un seul client
*/
void afficherCompte(compte compte) {
	cout << endl;
	cout << "Compte num�ro " << compte.noCompte << endl; 
	cout << "Propri�taire : " << compte.prenom << " " << compte.nom << endl;
	cout << "Solde du compte : " << fixed << setprecision(2) << compte.solde << "$" << endl;
}


/*
	T�che : affectuer une transaction bancaire sur un compte en fonction d'un type de transaction et d'un montant
	Param�tres : typeTransaction : le type de transaction effectu�
				 compte : repr�sente le compte d'un client
				 montant : le montant de la transaction

*/
void effectuerTransaction(TypeTransaction typeTransaction, compte &compte, double montant) {
	switch (typeTransaction) {
		case TypeTransaction::DEPOT:
			effectuerDepot(compte, montant);
			break;
		case TypeTransaction::RETRAIT:
			effectuerRetrait(compte, montant);
			break;
	}
}

/*
	T�che : affectuer un d�pot dans le compte d'un client.
	Param�tres : compte : repr�sente le compte d'un client
				 montant : le montant de la transaction
*/
void effectuerDepot(compte &compte, double montant) {
	cout << endl;
	compte.solde += montant;
	cout << "D�pot de " << fixed << setprecision(2) << montant << "$ effectu� avec succ�s dans le compte num�ro " << compte.noCompte << " !" << endl;
	cout << compte.prenom << " " << compte.nom << ", votre solde est maintenant de " << fixed << setprecision(2) << compte.solde << "$" << endl;
	signature();
}

/*
	T�che : affectuer un retrait dans le compte d'un client.
	Param�tres : compte : repr�sente le compte d'un client
				 montant : le montant de la transaction
*/
void effectuerRetrait(compte &compte, double montant) {
	cout << endl;
	compte.solde -= montant;
	cout << "Retrait de " << fixed << setprecision(2) << montant << "$ effectu� avec succ�s depuis le compte num�ro " << compte.noCompte << " !" << endl;
	cout << compte.prenom << " " << compte.nom << ", votre solde est maintenant de " << fixed << setprecision(2) << compte.solde << "$" << endl;
	signature();
}

void validerUnsignedInt(unsigned int &aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide" << endl << "--> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerUnsignedIntEntreBornes(unsigned int & aValider, int MIN, int MAX)
{
	validerUnsignedInt(aValider);
	while (!(aValider >= MIN && aValider <= MAX)) {
		cout << "ERREUR - Veuillez saisir une valeur entre " << MIN << " et " << MAX << " inclusivement." << endl << "--> ";
		validerUnsignedInt(aValider);
	}
}

void entrerPrenom(compte comptes[], unsigned int nbCompteActuel, const int TAILLE_PRENOM)
{
	cin.getline(comptes[nbCompteActuel].prenom, TAILLE_PRENOM);
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "D�sol�, votre pr�nom est trop long. Veuillez r�essayer en vous assurant de ne pas d�passer " << TAILLE_PRENOM << " caract�res" << endl << "--> ";
		cin.getline(comptes[nbCompteActuel].prenom, TAILLE_PRENOM);
	}
}

void entrerNom(compte comptes[], unsigned int nbCompteActuel, const int TAILLE_NOM)
{
	cin.getline(comptes[nbCompteActuel].nom, TAILLE_NOM);
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "D�sol�, votre nom de famille est trop long. Veuillez r�essayer en vous assurant de ne pas d�passer " << TAILLE_NOM << " caract�res" << endl << "--> ";
		cin.getline(comptes[nbCompteActuel].nom, TAILLE_NOM);
	}
}

void validerDouble(double & aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide" << endl << "--> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerDoublePositif(double & aValider)
{
	validerDouble(aValider);
	while (!(aValider >= 0)) {
		cout << "ERREUR - Vous ne pouvez pas saisir un montant n�gatif." << endl << "Veuillez saisir un montant d'au moins 0$" << endl << "--> ";
		validerDouble(aValider);
	}
}

void validerDoublePlusGrandQueZero(double & aValider)
{
	validerDouble(aValider);
	while (!(aValider > 0)) {
		cout << "ERREUR - Vous ne pouvez pas saisir une valeur nulle." << endl << "Veuillez saisir un montant plus grand que 0$" << endl << "--> ";
		validerDouble(aValider);
	}
}

void preparationDepot(compte comptes[], unsigned int noCompte)
{
	double montant;
	TypeTransaction depot = TypeTransaction::DEPOT;
	choixMontant(montant, depot);
	effectuerTransaction(depot, comptes[noCompte], montant);
}

void preparationRetrait(compte comptes[], unsigned int noCompte)
{
	double montant;
	TypeTransaction retrait = TypeTransaction::RETRAIT;
	choixMontant(montant, retrait);
	while (montant > comptes[noCompte].solde) {
		cout << "D�sol�, vous ne disposez pas d'assez de fond dans votre solde pour effectuer ce retrait." << endl;
		choixMontant(montant, retrait);
	}
	effectuerTransaction(retrait, comptes[noCompte], montant);
}

void verification(compte comptes[], unsigned int nbComptes, unsigned int saisie)
{
	cout << endl;
	if (nbComptes == 0) {
		cout << "Il n'existe encore aucun compte chez Jardibank." << endl;
		signature();
	}
	else {
		int nbComptesActifs = recupererNombreCompteActif(comptes, nbComptes);
		if (nbComptesActifs == 0) {
			cout << "Il n'existe aucun compte Jardibank actif." << endl;
			signature();
		}
		else {
			switch (saisie) {
				case 2:
				case 3:
				case 4:
				case 5:
					verificationBis(comptes, nbComptes, saisie);
					break;
				case 6:
					cout << "Il y a " << nbComptesActifs << " compte" << (nbComptesActifs > 1 ? "s" : "") << " Jardibank actif" << (nbComptesActifs > 1 ? "s" : "") << " :" << endl;
					afficherTousLesComptes(comptes, nbComptes);
					break;
				case 7:
					afficherSommeSoldes(comptes, nbComptes);
					break;
				case 8:
					afficherSoldeMoyen(comptes, nbComptes);
					break;
			}
		}
	}
}

void verificationBis(compte comptes[], unsigned int nbComptes, unsigned int saisie)
{
	unsigned int noCompte;
	noCompte = rechercherClient(comptes, nbComptes);
	if (noCompte != -1) {
		switch (saisie) {
			case 2: 
				desactiverCompte(comptes, noCompte);
				break;
			case 3:
				afficherCompte(comptes[noCompte]);
				signature();
				break;
			case 4:
				preparationDepot(comptes, noCompte);
				break;
			case 5:
				preparationRetrait(comptes, noCompte);
				break;
		}
	}
}

void choixMontant(double & montant, TypeTransaction transaction)
{
	cout << endl;
	cout << "Veuillez entrer un montant a " << (transaction == TypeTransaction::DEPOT ? "deposer dans" : "retirer de") << " votre compte " << endl << "--> ";
	validerDoublePlusGrandQueZero(montant);
}

double calculerSoldeMoyen(compte comptes[], unsigned int nbComptes)
{
	double somme = calculerSommeSoldes(comptes, nbComptes);
	int nbComptesActifs = recupererNombreCompteActif(comptes, nbComptes);
	double soldeMoyen = somme / nbComptesActifs;
	return soldeMoyen;
}

void signature()
{
	cout << "-----" << endl;
	cout << "A bientot chez" << endl;
	cout << "~*~ JARDIBANK ~*~" << endl;
	cout << endl << endl;
}
