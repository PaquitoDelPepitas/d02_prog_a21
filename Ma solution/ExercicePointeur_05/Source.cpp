#include <iostream>
using namespace std;

void echanger(int& a, int& b);

int main() {
	int val1 = 10, val2 = 20;
	cout << val1 << " " << val2 << endl;
	echanger(val1, val2);
	cout << val1 << " " << val2 << endl;

	system("pause");
	return 0;
}

void echanger(int& a, int& b)
{
	int c = a;
	a = b;
	b = c;
}

/*
	Ce code affichera :
	10 20
	20 10

	Les variables etant pass�es a la fonction par reference, elles seront modifi�es partout directement, et pas uniquement dans la fonction
*/