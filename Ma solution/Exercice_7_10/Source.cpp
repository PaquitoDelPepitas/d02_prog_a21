#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int accident;
	double anciennePrime, augmentation, nouvellePrime;

	const double AUGMENTATION_MIN = 0.02, AUGMENTATION_PETITE = 0.05, AUGMENTATION_GRANDE = 0.1, AUGMENTATION_MAX = 0.3;
	
	const int ACCIDENT_PALLIER_2 = 1, ACCIDENT_PALLIER_3 = 3, ACCIDENT_PALLIER_4 = 4;

	cout << "Quelle est l'ancienne valeur de la prime ? ";
	cin >> anciennePrime;
	cout << "Combien d'accident a eu le client ? ";
	cin >> accident;

	if (accident >= ACCIDENT_PALLIER_4) {
		augmentation = AUGMENTATION_MAX;
	}
	else if (accident == ACCIDENT_PALLIER_3) {
		augmentation = AUGMENTATION_GRANDE;
	}
	else if (accident == ACCIDENT_PALLIER_2) {
		augmentation = AUGMENTATION_PETITE;
	}
	else {
		augmentation = AUGMENTATION_MIN;
	}

	nouvellePrime = anciennePrime + (anciennePrime * augmentation);
	cout << "La nouvelle valeur de la prime s'�l�ve a " << nouvellePrime << "$" << endl;

	system("pause");
	return 0;
}