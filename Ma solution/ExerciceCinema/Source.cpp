#include <iostream>;
using namespace std;

enum JourDeLaSemaine {
	LUNDI = 1,
	MARDI,
	MERCREDI,
	JEUDI,
	VENDREDI,
	SAMEDI,
	DIMANCHE
};

int main() {
	setlocale(LC_ALL, "");

	int age;

	cout << "Quel est votre age ? ";
	cin >> age;
	while (cin.fail() || cin.peek() != '\n' || age < 0) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Veuillez saisir un age valide : ";
		cin >> age;
	}

	unsigned short jourSaisi;

	cout << "Quel jour sommes-nous ?" << endl;
	cout << "(" << JourDeLaSemaine::LUNDI << ") Lundi" << endl;
	cout << "(" << JourDeLaSemaine::MARDI << ") Mardi" << endl;
	cout << "(" << JourDeLaSemaine::MERCREDI << ") Mercredi" << endl;
	cout << "(" << JourDeLaSemaine::JEUDI << ") Jeudi" << endl;
	cout << "(" << JourDeLaSemaine::VENDREDI << ") Vendredi" << endl;
	cout << "(" << JourDeLaSemaine::SAMEDI << ") Samedi" << endl;
	cout << "(" << JourDeLaSemaine::DIMANCHE << ") Dimanche" << endl;
	cout << "Saisir le chiffre correspondant au jour d'aujourd'hui : ";
	cin >> jourSaisi;
	while (cin.fail() || cin.peek() != '\n' || !(jourSaisi >= 1 && jourSaisi <= 7)) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Erreur - Veuillez choisir un des jours de la semaine : ";
		cin >> jourSaisi;
	}
	int jour = (JourDeLaSemaine)jourSaisi;

	double reduction, prixFinal;
	const int AGE_MIN = 16, AGE_MAX = 65;
	const double PRIX_BASE = 10, REDUC_MIN = 0.1, REDUC_MID = 0.2, REDUC_MAX = 0.3;

	switch (jour) {
		case 1:
			reduction = (age < AGE_MIN || age > AGE_MAX) ? REDUC_MIN : REDUC_MID;
			break;
		case 2:
		case 3:
			reduction = (age < AGE_MIN || age > AGE_MAX) ? REDUC_MAX : 0;
			break;
		case 4:
			reduction = (age < AGE_MIN || age > AGE_MAX) ? REDUC_MAX : REDUC_MID;
			break;
		case 5:
		case 6:
		case 7:
			reduction = (age < AGE_MIN || age > AGE_MAX) ? REDUC_MIN : 0;
			break;
	}

	prixFinal = PRIX_BASE - (PRIX_BASE * reduction);

	cout << "Vous avez " << age << " ans, et nous sommes "; 
	switch (jour) {
		case 1:
			cout << "Lundi.";
			break;
		case 2:
			cout << "Mardi.";
			break;
		case 3:
			cout << "Mercredi.";
			break;
		case 4:
			cout << "Jeudi.";
			break;
		case 5:
			cout << "Vendredi.";
			break;
		case 6:
			cout << "Samedi.";
			break;
		case 7:
			cout << "Dimanche.";
			break;
	}

	if (reduction == 0.1) {
		cout << " Vous b�n�ficiez d'une r�duction de 10%. ";
	}
	else if (reduction == 0.2) {
		cout << " Vous b�n�ficiez d'une r�duction de 20%. ";
	}
	else if (reduction == 0.3) {
		cout << " Vous b�n�ficiez d'une r�duction de 30%. ";
	}
	else {
		cout << " Vous ne b�n�ficiez pas de r�duction. ";
	}
	cout << "Le prix de votre billet est de " << prixFinal << "$" << endl << "Cin�Benji vous souhaite une bonne s�ance !" << endl;

	system("pause");
	return 0;
}