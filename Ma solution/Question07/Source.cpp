#include <iostream>
using namespace std;

enum Mois {
	JANVIER = 0,
	FEVRIER,
	MARS,
	AVRIL,
	MAI,
	JUIN,
	JUILLET,
	AOUT,
	SEPTEMBRE,
	OCTOBRE,
	NOVEMBRE,
	DECEMBRE
};

void afficherMois(int val);

int main() {
	setlocale(LC_ALL, "");

	afficherMois(0);

	system("pause");
	return 0;
}

void afficherMois(int val)
{
	switch (val) {
		case Mois::JANVIER:
			cout << "Janvier";
			break;
		case Mois::FEVRIER:
			cout << "F�vrier";
			break;
		case Mois::MARS:
			cout << "Mars";
			break;
		case Mois::AVRIL:
			cout << "Avril";
			break;
		case Mois::MAI:
			cout << "Mai";
			break;
		case Mois::JUIN:
			cout << "Juin";
			break;
		case Mois::JUILLET:
			cout << "Juillet";
			break;
		case Mois::AOUT:
			cout << "Aout";
			break;
		case Mois::SEPTEMBRE:
			cout << "Septembre";
			break;
		case Mois::OCTOBRE:
			cout << "Octobre";
			break;
		case Mois::NOVEMBRE:
			cout << "Novembre";
			break;
		case Mois::DECEMBRE:
			cout << "D�cembre";
			break;
		default:
			cout << "Mois introuvable.";
			break;
	}
	cout << endl;
}
