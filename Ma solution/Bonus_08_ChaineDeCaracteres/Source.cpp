#include <iostream>
using namespace std;

void recupererLongueurChaine(char chaine[], int &longueur);

int occurrences(char chaine[], char c);
void inverser(char chaine[]);
void copier(char destination[], char source[]);
void concatener(char destination[], char source[]);
void tailler(char chaine[]);
int comparer(char chaine1[], char chaine2[]);

int main() {
	setlocale(LC_ALL, "");

	/*
	    1) �crire une fonction qui re�oit une cha�ne de caract�res et un caract�re, et qui retourne le nombre de fois que ce caract�re apparait dans la cha�ne.
		ex: si elle re�oit "Bonjour" et 'o', elle retourne 2.
	*/
	cout << "EXERCICE 1" << endl;
	char chaineEx1[] = "Salut tout le monde !";
	char c = 't';
	cout << "Chaine test�e : " << chaineEx1 << endl;
	cout << "Caract�re choisi : " << c << endl;
	cout << endl;
	int occurence = occurrences(chaineEx1, c); 
	cout << "Il y a " << occurence << " occurences du caract�re " << c << " dans la chaine test�e" << endl;
	cout << endl;
	cout << "----------" << endl;
	cout << endl;

	/*
	    2) �crire une fonction qui re�oit une cha�ne de caract�res et inverse la cha�ne en m�moire.
		ex: si elle re�oit "Bonjour", la cha�ne contiendra "ruojnoB" � la suite de l'appel.
	*/
	cout << "EXERCICE 2" << endl;
	char chaineEx2[] = "Hello World !";
	cout << "Before : " << chaineEx2 << endl;
	inverser(chaineEx2);
	cout << "After : " << chaineEx2;
	cout << endl << endl;
	cout << "----------" << endl;
	cout << endl;

	/*
	    3) �crire une fonction qui re�oit deux cha�nes de caract�res et qui copie la seconde dans la premi�re.
		ex: si elle re�oit "Bonjour" et "Les gens", apr�s l'appel la premi�re contiendra "Les gens".
	*/
	cout << "EXERCICE 3" << endl;
	const int TAILLE = 100;
	char chaineDesination[TAILLE] = "Remplacement";
	char chaineSource[TAILLE] = "Ceci va etre copi� !";
	cout << "Destination : " << chaineDesination << endl;
	cout << "Source : " << chaineSource << endl;
	copier(chaineDesination, chaineSource);
	cout << "Nouvelle destination : " << chaineDesination;
	cout << endl << endl;
	cout << "----------" << endl;
	cout << endl;

	/*
	    4) �crire une fonction qui re�oit deux cha�nes de caract�res et qui copie les caract�res de la seconde � la fin de la premi�re.
		ex: si elle re�oit "Bonjour" et "tout le monde", apr�s l'appel la premi�re cha�ne contiendra "Bonjourtout le monde".
	*/
	cout << "EXERCICE 4" << endl;
	char chaineD[TAILLE] = "Bonjour";
	char chaineS[TAILLE] = "tout le monde";
	cout << "Destination : " << chaineD << endl;
	cout << "Source : " << chaineS << endl;
	concatener(chaineD, chaineS);
	cout << "Nouvelle destination : " << chaineD;
	cout << endl << endl;
	cout << "----------" << endl;
	cout << endl;

	/* 
		5) �crire une fonction qui re�oit une cha�ne et enl�ve les espaces inutiles au d�but et � la fin.
		ex: si elle re�oit "    Je suis une cha�ne.    ", apr�s l'appel, la cha�ne contiendra "Je suis une cha�ne."
	*/
	cout << "EXERCICE 5" << endl;
	char chaineEx5[] = "   Salut   ";
	cout << "Before : -" << chaineEx5 << "!" << endl;
	tailler(chaineEx5);
	cout << "After : -" << chaineEx5 << "!";
	cout << endl << endl;
	cout << "----------" << endl;
	cout << endl;

	/*
	6) �crire une fonction qui re�oit deux cha�nes de caract�res et qui les compare en utilisant l'ordre lexicographique (ordre du dictionnaire). Elle retourne 0 si les cha�nes sont identiques, 1 si la premi�re est plus grande que la seconde et -1 sinon.
	ex: si elle re�oit "Ceci est une cha�ne" et "Ceci est une chemise.", elle retournera - 1.
	*/
	cout << "EXERCICE 6" << endl;
	char chaine1[] = "Ceci est une chaine";
	char chaine2[] = "Ceci est une chemise";
	cout << "Premi�re chaine : " << chaine1 << endl;
	cout << "Deuxi�me chaine : " << chaine2 << endl;
	int resultat = comparer(chaine1, chaine2);
	cout << "R�sultat : " << resultat;
	cout << endl << endl;
	cout << "----------" << endl;
	cout << endl;

	system("pause");
	return 0;
}

void recupererLongueurChaine(char chaine[], int & longueur)
{
	longueur = 0;
	while (chaine[longueur] != NULL) {
		longueur++;
	}
}

int occurrences(char chaine[], char c)
{
	int longueur;
	recupererLongueurChaine(chaine, longueur);
	int cpt = 0;
	for (int i = 0; i < longueur; i++) {
		cpt += (chaine[i] == c ? 1 : 0);
	}
	return cpt;
}

void inverser(char chaine[])
{
	int longueur;
	recupererLongueurChaine(chaine, longueur);
	for (int i = 0; i < longueur / 2; i++) {
		char save = chaine[i];
		chaine[i] = chaine[longueur - i - 1];
		chaine[longueur - i - 1] = save;
	}
}

void copier(char destination[], char source[])
{
	int longueur;
	recupererLongueurChaine(source, longueur);
	for (int i = 0; i <= longueur; i++) {
		destination[i] = source[i];
	}
}

void concatener(char destination[], char source[])
{
	int longueurD, longueurS;
	recupererLongueurChaine(destination, longueurD);
	recupererLongueurChaine(source, longueurS);
	for (int i = 0; i < longueurS; i++) {
		destination[longueurD + i] = source[i];
	}
}

void tailler(char chaine[])
{
	int longueur;
	recupererLongueurChaine(chaine, longueur);
	int i = 0;
	while(chaine[i] == ' ') {
		for (int j = 0; j < longueur; j++) {
			chaine[j] = chaine[j + 1];
		}
	}
	recupererLongueurChaine(chaine, longueur);
	int cpt = 0;
	for (int k = 0; k < longueur; k++) {
		cpt += (chaine[k] == ' ' ? 1 : 0);
	}
	chaine[longueur - cpt] = NULL;
}

int comparer(char chaine1[], char chaine2[])
{
	int longueur1, longueur2;
	recupererLongueurChaine(chaine1, longueur1);
	recupererLongueurChaine(chaine2, longueur2);

	int i = 0;
	int c1 = chaine1[i], c2 = chaine2[i];
	while (c1 == c2 && i < longueur1 && i < longueur2) {
		c1 = chaine1[i];
		c2 = chaine2[i];
		i++;
	}

	int retour;
	if (c1 > c2) {
		retour = 1;
	}
	else if (c2 > c1) {
		retour = -1;
	}
	else {
		retour = 0;
	}

	return retour;
}
