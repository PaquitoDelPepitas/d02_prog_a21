#include <iostream>
using namespace std;

struct article {
	char nom[101];
	unsigned int quantite;
	double prix;
};

double calculerSousTotal(double prix, unsigned int qte);
double calculerTaxeQuebec(double sousTotal, double TPS, double TVQ);
void afficherArticle(article & article);

void validerChaineCaractere(char chaine[], unsigned short max);
void validerUnsignedInt(unsigned int & aValider);
void validerDouble(double & aValider);
void validerDoublePositif(double & aValider);

int main() {
	setlocale(LC_ALL, "");

	article article;

	cout << "SAISIE D'UN NOUVEL ARTICLE" << endl;

	cout << "Nom --> ";
	validerChaineCaractere(article.nom, 101);

	cout << "Qt� --> ";
	validerUnsignedInt(article.quantite);

	cout << "Prix unitaire --> ";
	validerDoublePositif(article.prix);

	cout << endl;

	afficherArticle(article);

	system("pause");
	return 0;
}

double calculerSousTotal(double prix, unsigned int qte)
{
	double sousTotal = prix * qte;
	return sousTotal;
}

double calculerTaxeQuebec(double sousTotal, double TPS, double TVQ)
{
	double taxe = (sousTotal * TPS) + (sousTotal * TVQ);
	return taxe;
}

void afficherArticle(article & article)
{
	cout << "FICHE ARTICLE" << endl;
	cout << "Nom : " << article.nom << endl;
	cout << "Qt� : " << article.quantite << endl;
	cout << "Prix unitaire : " << article.prix << "$" << endl;
	cout << endl;

	double sousTotal = calculerSousTotal(article.prix, article.quantite);
	cout << "Sous total : " << sousTotal << "$" << endl;

	const double TPS = 0.05, TVQ = 0.09975;
	double taxe = calculerTaxeQuebec(sousTotal, TPS, TVQ);
	cout << "Taxes : " << taxe << "$" << endl;

	cout << endl;
}

void validerChaineCaractere(char chaine[], unsigned short max)
{
	cin.getline(chaine, max);
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Votre saisie est trop longue. Veuillez effectuer une nouvelle saisie en respectant la limite de " << max << "caract�res" << endl;
		cin.getline(chaine, max);
	}
}

void validerUnsignedInt(unsigned int & aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide --> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerDouble(double & aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide --> ";
		cin >> aValider;
	}
	cin.ignore(512, '\n');
}

void validerDoublePositif(double & aValider)
{
	validerDouble(aValider);
	while (!(aValider > 0)) {
		cout << "ERREUR - Le prix ne peut etre nul ou n�gatif ! Veuillez saisir un prix correcte." << endl;
		cout << "--> ";
		validerDouble(aValider);
	}
}
