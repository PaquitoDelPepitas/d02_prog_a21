#include <iostream>
#include <time.h>
using namespace std;

const unsigned int TAILLE_COMBINAISON = 6;
const unsigned int RANDOM_MIN = 1, RANDOM_MAX = 49;

void afficherMenu(unsigned int combiJoueur[], unsigned int combiGagnante[], unsigned int MIN, unsigned int MAX);

void saisirCombinaison(unsigned int combiJoueur[], unsigned int MIN, unsigned int MAX);
void genererCombinaison(unsigned int combi[], unsigned int MIN, unsigned int MAX);

void trierCombinaison(unsigned int combi[]);
void afficherCombinaisons(unsigned int combiJoueur[], unsigned int combiGagnante[]);
void comparerCombinaisons(unsigned int combiJoueur[], unsigned int combiGagnante[]);

void validerUnsignedInt(unsigned int &aValider);
void validerUnsignedIntEntreBornes(unsigned int &aValider, unsigned int MIN, unsigned int MAX);

int main() {
	setlocale(LC_ALL, "");
	srand(time(0));

	unsigned int combiJoueur[TAILLE_COMBINAISON], combiGagnante[TAILLE_COMBINAISON];

	afficherMenu(combiJoueur, combiGagnante, RANDOM_MIN, RANDOM_MAX);

	return 0;
}

void afficherMenu(unsigned int combiJoueur[], unsigned int combiGagnante[], const unsigned int MIN, const unsigned int MAX)
{
	unsigned int saisie;

	cout << "~*~ SUPER LOTO ~*~" << endl;
	do {
		cout << endl;
		cout << "Comment souhaitez-vous jouer ?" << endl;
		cout << "(1) - Saisir ma combinaison" << endl;
		cout << "(2) - G�nerer une combinaison al�atoire" << endl;
		cout << "-----" << endl;
		cout << "(3) - Quitter le jeu" << endl;
		cout << endl;
		cout << "--> ";

		const unsigned int MIN_MENU = 1, MAX_MENU = 3;
		validerUnsignedIntEntreBornes(saisie, MIN_MENU, MAX_MENU);

		switch (saisie) {
			case 1:
				saisirCombinaison(combiJoueur, MIN, MAX);
				break;
			case 2:
				genererCombinaison(combiJoueur, MIN, MAX);
				break;
			default:
				break;
		}

		genererCombinaison(combiGagnante, MIN, MAX);
		trierCombinaison(combiJoueur);
		trierCombinaison(combiGagnante);
		afficherCombinaisons(combiJoueur, combiGagnante);
		comparerCombinaisons(combiJoueur, combiGagnante);

	} while (saisie != 3);
}

void saisirCombinaison(unsigned int combiJoueur[], unsigned int MIN, unsigned int MAX)
{
	cout << endl;
	cout << "Veuillez saisir votre combinaison de " << TAILLE_COMBINAISON << " nombres :" << endl;
	for (int i = 0; i < TAILLE_COMBINAISON; i++) {
		cout << i + 1 << "e nombre --> ";
		unsigned int saisie;
		validerUnsignedIntEntreBornes(saisie, MIN, MAX);
		combiJoueur[i] = saisie;
	}
}

void genererCombinaison(unsigned int combi[], unsigned int MIN, unsigned int MAX)
{
	unsigned short boules[RANDOM_MAX], compteur = 1;
	for (int j = 0; j < MAX; j++) {
		boules[j] = j + 1;
	}
	for (int i = 0; i < TAILLE_COMBINAISON; i++) {
		int randomIndex = rand() % (MAX - compteur);
		combi[i] = boules[randomIndex];
		boules[randomIndex] = boules[MAX - compteur];
		compteur++;
	}
}

void trierCombinaison(unsigned int combi[])
{
	unsigned int compteur = 0;
	bool permutation;
	do {
		permutation = false;
		for (int i = 0; i < TAILLE_COMBINAISON - compteur; i++) {
			if (combi[i] > combi[i + 1]) {
				unsigned int val = combi[i];
				combi[i] = combi[i + 1];
				combi[i + 1] = val;
				permutation = true;
			}
		}
		compteur++;
	} while (permutation);
}

void afficherCombinaisons(unsigned int combiJoueur[], unsigned int combiGagnante[])
{
	cout << endl;
	cout << "COMBINAISON GAGNANTE" << endl;
	for (int i = 0; i < TAILLE_COMBINAISON; i++) {
		cout << combiGagnante[i] << "  ";
	}
	cout << endl;
	for (int j = 0; j < TAILLE_COMBINAISON; j++) {
		cout << combiJoueur[j] << "  ";
	}
	cout << endl;
	cout << "COMBINAISON JOUEE" << endl;
}

void comparerCombinaisons(unsigned int combiJoueur[], unsigned int combiGagnante[])
{
	int compteur = 0;
	for (int i = 0; i < TAILLE_COMBINAISON; i++) {
		for (int j = 0; j < TAILLE_COMBINAISON; j++) {
			if (combiGagnante[i] == combiJoueur[j]) {
				compteur++;
			}
		}
	}
	cout << endl;
	cout << "Vous avez " << compteur << " nombre" << (compteur > 1 ? "s" : "") << " en commun avec la combinaison gagnante." << endl;
	cout << "-----" << endl;
	cout << "Merci d'avoir jou�, a bientot !" << endl << endl;
}

void validerUnsignedInt(unsigned int &aValider) {
	cin >> aValider;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "ERREUR - Veuillez saisir une valeur valide : ";
		cin >> aValider;
	}
}

void validerUnsignedIntEntreBornes(unsigned int & aValider, unsigned int MIN, unsigned int MAX)
{
	validerUnsignedInt(aValider);
	while (!(aValider >= MIN && aValider <= MAX)) {
		cout << "ERREUR - Vous devez saisir une valeur entre " << MIN << " et " << MAX << " : ";
		validerUnsignedInt(aValider);
	}
}
