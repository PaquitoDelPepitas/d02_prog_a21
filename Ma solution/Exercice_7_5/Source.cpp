#include <iostream>;
using namespace std; 

int main() {
	setlocale(LC_ALL, "");

	double soldeActuel, retrait, soldeRestant;

	cout << "Veuillez renseigner le solde actuel de votre compte : ";
	cin >> soldeActuel; 

	cout << "Combien voulez-vous retirer ? ";
	cin >> retrait;

	if (retrait <= soldeActuel) {
		soldeRestant = soldeActuel - retrait;
		cout << "Votre solde est désormais de " << soldeRestant << "$" << endl;
	}
	else {
		cout << "Vous ne disposez pas d'assez de fond dans votre votre compte en banque." << endl;
	}

	system("pause");
	return 0;
}