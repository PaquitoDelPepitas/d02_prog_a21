#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int multiplicateur = 2, produit = 1, valeurSaisie, factoriel;

	factoriel = multiplicateur - 1;

	cout << "Veuillez saisir une valeur enti�re : ";
	cin >> valeurSaisie;

	while (produit < valeurSaisie) {
		produit = produit * multiplicateur;
		multiplicateur++;
		factoriel++;
	}

	if (produit != valeurSaisie) {
		cout << "Il n'y a pas de factoriel pour le nombre " << valeurSaisie << endl;
	}
	else {
		cout << "La factoriel est " << factoriel << endl << endl; 
		cout << "Validation : " << endl;
		
		multiplicateur = 2;
		int resultat = 0, resultatPrecedent = 1;

		while (resultat != valeurSaisie) {
			resultat = resultatPrecedent * multiplicateur;
			cout << resultatPrecedent << " * " << multiplicateur << " = " << resultat << endl;
			resultatPrecedent = resultat;
			multiplicateur++;
		}
	}

	system("pause");
	return 0;
}