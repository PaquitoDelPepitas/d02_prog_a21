#include <iostream>;
#include <iomanip>;
using namespace std;

enum Mois {
	JANVIER, FEVRIER, MARS, AVRIL, MAI, JUIN, JUILLET, AOUT, SEPTEMBRE, OCTOBRE, NOVEMBRE, DECEMBRE
};

struct date {
	unsigned int jour, annee;
	Mois mois;
};

struct avion {
	double capaciteReservoir;
	unsigned int nombrePlaceAssise;
	date dateMiseEnService;
};

int main() {
	setlocale(LC_ALL, "");

	bool continuer;
	unsigned int totalPlaceAssise = 0;
	double sommeCapaciteCarburant = 0, moyenneCarburant;
	unsigned int compteurAvion = 0;
	unsigned int saisiMois;

	unsigned int jourAncien = 1000000000, moisAncien = 1000000000, anneeAncienne = 1000000000;

	avion ancienAvion;

	do {
		avion thisAvion;
		compteurAvion += 1;

		cout << "Remplir la fiche d'information de l'avion." << endl << "Capacit� du r�servoir : ";
		cin >> thisAvion.capaciteReservoir;
		cout << "Nombre de place assise : ";
		cin >> thisAvion.nombrePlaceAssise;
		cout << "Date de mise en service (JJ/MM/AAAA) : ";
		cin >> thisAvion.dateMiseEnService.jour >> saisiMois >> thisAvion.dateMiseEnService.annee;

		totalPlaceAssise += thisAvion.nombrePlaceAssise;
		sommeCapaciteCarburant += thisAvion.capaciteReservoir;

		if (saisiMois == Mois::JANVIER) {
			thisAvion.dateMiseEnService.mois = JANVIER;
		}
		else if (saisiMois == Mois::FEVRIER) {
			thisAvion.dateMiseEnService.mois = FEVRIER;
		}
		else if (saisiMois == Mois::MARS) {
			thisAvion.dateMiseEnService.mois = MARS;
		}
		else if (saisiMois == Mois::AVRIL) {
			thisAvion.dateMiseEnService.mois = AVRIL;
		}
		else if (saisiMois == Mois::MAI) {
			thisAvion.dateMiseEnService.mois = MAI;
		}
		else if (saisiMois == Mois::JUIN) {
			thisAvion.dateMiseEnService.mois = JUIN;
		}
		else if (saisiMois == Mois::JUILLET) {
			thisAvion.dateMiseEnService.mois = JUILLET;
		}
		else if (saisiMois == Mois::AOUT) {
			thisAvion.dateMiseEnService.mois = AOUT;
		}
		else if (saisiMois == Mois::SEPTEMBRE) {
			thisAvion.dateMiseEnService.mois = SEPTEMBRE;
		}
		else if (saisiMois == Mois::OCTOBRE) {
			thisAvion.dateMiseEnService.mois = OCTOBRE;
		}
		else if (saisiMois == Mois::NOVEMBRE) {
			thisAvion.dateMiseEnService.mois = NOVEMBRE;
		}
		else if (saisiMois == Mois::DECEMBRE) {
			thisAvion.dateMiseEnService.mois = DECEMBRE;
		}

		if (thisAvion.dateMiseEnService.annee < anneeAncienne) {
			ancienAvion = thisAvion;
		} 
		else if (thisAvion.dateMiseEnService.annee == anneeAncienne) {
			if (saisiMois < moisAncien) {
				ancienAvion = thisAvion;
			}
			else if (saisiMois == moisAncien) {
				if (thisAvion.dateMiseEnService.jour < jourAncien) {
					ancienAvion = thisAvion;
				}
			}
		}

		cout << endl << "Fiche d'informations de l'avion" << endl << "Capacit� du r�servoir : " << thisAvion.capaciteReservoir << endl << "Nombre de place assise : " << thisAvion.nombrePlaceAssise << endl << "Date de mise en service : " << thisAvion.dateMiseEnService.jour << "/";

		if (saisiMois == Mois::JANVIER) {
			cout << "janvier";
		}
		else if (saisiMois == Mois::FEVRIER) {
			cout << "fevrier";
		}
		else if (saisiMois == Mois::MARS) {
			cout << "mars";
		}
		else if (saisiMois == Mois::AVRIL) {
			cout << "avril";
		}
		else if (saisiMois == Mois::MAI) {
			cout << "mai";
		}
		else if (saisiMois == Mois::JUIN) {
			cout << "juin";
		}
		else if (saisiMois == Mois::JUILLET) {
			cout << "juillet";
		}
		else if (saisiMois == Mois::AOUT) {
			cout << "aout";
		}
		else if (saisiMois == Mois::SEPTEMBRE) {
			cout << "septembre";
		}
		else if (saisiMois == Mois::OCTOBRE) {
			cout << "octobre";
		}
		else if (saisiMois == Mois::NOVEMBRE) {
			cout << "novembre";
		}
		else if (saisiMois == Mois::DECEMBRE) {
			cout << "decembre";
		}

		cout << "/" << thisAvion.dateMiseEnService.annee << endl;

		cout << endl << "Voulez-vous saisir un nouvel avion ? (1 = OUI, 0 = NON) ";
		cin >> continuer;
		cout << endl;
	} while (continuer);

	moyenneCarburant = sommeCapaciteCarburant / compteurAvion;

	cout << "Il y a un total de " << totalPlaceAssise << " places assises pour tous les avions saisis pour une capacit� moyenne de carburant de " << fixed << setprecision(2) << moyenneCarburant << "L." << endl;

	cout << endl << "Fiche d'informations de l'avion le plus ancien" << endl << "Capacit� du r�servoir : " << ancienAvion.capaciteReservoir << endl << "Nombre de place assise : " << ancienAvion.nombrePlaceAssise << endl << "Date de mise en service : " << ancienAvion.dateMiseEnService.jour << "/";
	
	if (ancienAvion.dateMiseEnService.mois == Mois::JANVIER) {
		cout << "janvier";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::FEVRIER) {
		cout << "fevrier";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::MARS) {
		cout << "mars";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::AVRIL) {
		cout << "avril";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::MAI) {
		cout << "mai";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::JUIN) {
		cout << "juin";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::JUILLET) {
		cout << "juillet";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::AOUT) {
		cout << "aout";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::SEPTEMBRE) {
		cout << "septembre";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::OCTOBRE) {
		cout << "octobre";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::NOVEMBRE) {
		cout << "novembre";
	}
	else if (ancienAvion.dateMiseEnService.mois == Mois::DECEMBRE) {
		cout << "decembre";
	}
		
	cout << "/" << ancienAvion.dateMiseEnService.annee << endl;

	

	system("pause");
	return 0;
}