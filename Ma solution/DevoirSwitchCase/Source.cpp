#include <iostream>
using namespace std;

int main() {
	char animal;
	short couleur;
	cout << "Quel animal? (C)hien ou Ch(a)t : ";
	cin >> animal;
	cout << "Choisir une couleur (0 = noir, 1 = brun, 2 = blanc): ";
	cin >> couleur;
	//remplacer par un if..else if..else
	
	/*
		switch (animal)
		{
		case 'C':
		case 'c':
			cout << "Vous avez choisi un chien ";
			break;
		case 'A':
		case 'a':
			cout << "Vous avez choisi un chat ";
			break;
		}
	*/

	if (animal == 'C' || animal == 'c') {
		cout << "Vous avez choisi un chien !" << endl; 
	}
	else if(animal == 'A' || animal == 'a'){
		cout << "Vous avez choisi un chat !" << endl;
	}

	//remplacer par un switch..case
	/*
		if (couleur == 2) {
			cout << "blanc" << endl;
		}
		else if (couleur == 1) {
			cout << "brun" << endl;
		}
		else {
			cout << "noir" << endl;
		}
	*/

	switch (couleur) {
		case 1:
			cout << "brun" << endl;
			break;
		case 2: 
			cout << "blanc" << endl;
			break;
		default:
			cout << "noir" << endl;
			break;
	}

	system("pause");
	return 0;
}