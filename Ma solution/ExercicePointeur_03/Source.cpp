#include <iostream>
using namespace std;

void echanger(int a, int b);

int main() {
	int val1 = 10, val2 = 20;
	cout << val1 << " " << val2 << endl;
	echanger(val1, val2);
	cout << val1 << " " << val2 << endl;
	
	system("pause");
	return 0;
}

void echanger(int a, int b)
{
	int c = a;
	a = b;
	b = c;
}

/*
	Le bout de code suivante affichera : 
	10 20
	10 20 

	Les deux variables ne seront pas echang�s car elles sont pass�es par copie dans la fonction echanger(), donc leurs modifications ne s'operera que dans la fonction et pas en dehors
*/
