#include <iostream>
using namespace std;

int main() {
	bool reponse;
	cout << "Voulez-vous afficher un message? (0 = non, 1 = oui) : ";
	cin >> reponse;

	while (cin.fail() || cin.peek() != '\n') {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - Vous devez saisir 0 ou 1 : ";
		cin >> reponse;
	}

	if (reponse == true) {
		cout << "Voici le message!!!" << endl;
	}

	system("pause");
	return 0;
}