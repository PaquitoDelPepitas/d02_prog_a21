#include <iostream>
using namespace std;

const int TAILLE_TAB = 10;

unsigned int compterNombreNegatifDansTableau(int tab[]);

int main() {
	setlocale(LC_ALL, "");

	int tab[TAILLE_TAB] = { -1, 2, -3, 4, -5, 6, -7, 8, -9, 10 };
	int cpt = compterNombreNegatifDansTableau(tab);
	cout << "Nb de nombre n�gatif : " << cpt << endl;

	system("pause");
	return 0;
}

unsigned int compterNombreNegatifDansTableau(int tab[])
{
	unsigned int cpt = 0;

	for (int i = 0; i < TAILLE_TAB; i++) {
		cpt += (tab[i] < 0 ? 1 : 0);
	}

	return cpt;
}
