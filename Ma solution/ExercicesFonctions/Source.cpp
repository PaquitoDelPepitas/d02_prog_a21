#include <iostream>;
using namespace std;

/*
	Syntaxe prototype 
	[const]typeDeRetour nomFonction([parametre(s) formel(s)]);
*/

// Exercice 1 
int somme(int a, int b);

// Exercice 2
double volumeParallelepipede(double longueur, double largeur, double hauteur);

// Exercice 3
int plusGrand(int a, int b);

// Exercice 4
int somme(const int MIN, const int MAX); 

// Exercice 5
int saisirEntier();

int main() {
	return 0;
}

int somme(int a, int b)
{
	return a + b;
}
