#include <iostream>
using namespace std; 

void uneFonction(int a, int b, int& c, int* d, int e);

int main() {
	int val1 = 4, val2 = 2, reponse1, reponse2, reponse3 = 0;
	uneFonction(val1, val2, reponse1, &reponse2, reponse3);
	cout << reponse1 << endl;
	cout << reponse2 << endl;
	cout << reponse3 << endl;

	system("pause");
	return 0;
}

void uneFonction(int a, int b, int& c, int* d, int e)
{
	c = a + b;
	*d = a * b;
	e = a - b;
}

/* Questions A - E
	int a --> par copie 
	int b --> par copie 
	int c --> par reference 
	int d --> par adresse 
	int e --> par copie 
*/

/* Questions F
	Cette ligne affichera 6 car la valeur de la variable reponse1 va etre modifi�e directement grace a la reference de la variable
*/

/* Questions G
	Cette ligne affichera 8 car la valeur de la variable reponse2 va etre modifi�e directement grace a l'adresse de la variable
*/

/* Questions H
	Cette ligne affichera 0 car la valeur de la variable reponse3 ne sera pas chang�. Elle est pass�e par copie dans la fonctionm donc elle ne changera que dans la fonction et pas ailleurs
*/
