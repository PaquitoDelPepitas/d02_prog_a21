#include <iostream>;
using namespace std;

int main() {
	setlocale(LC_ALL, "");

	int resultat1 = 20 / 3;
	cout << resultat1 << endl;
	/* On obtient 6 
		Nous n'obtenons ici que la pqrtie entiere du resultat, 
		les d�cimales se perdent a cause du type utilis�
	*/

	double resultat2 = 20 / 3;
	cout << resultat2 << endl;
	/* On obtient egalement 6 
		Nous obtenons ici le meme resultat que precedemment. 
		Ca signifie peut etre que, malgr� la variable resultat2 qui est de type double,
		les nombres 20 et 3 sont peut etre reconnu comme etant de type int. 
		Ainsi, le resultat de leur operation sera un type int, donc sans decimal 
	*/

	double resultat3 = (double)20 / 3;
	cout << resultat3 << endl;
	/* On obtient 6.66667 
		Ca confirme peut etre l'hypothese precedente concernent les types de 20 et 3. 
		Ici on a forc� le type de 20 comme etant un type double. Ce qui veut dire que
		le resultat d'une operation entre un double et un int retourne un double 
	*/

	double resultat4 = (double)(20 / 3);
	cout << resultat4 << endl;
	/* On obtient 6
		Ici, on a beau preciser que l'on veut que le resultat de 20/3 soit un double,
		l'operation est realis� avant entre deux int, donc les decimales ne sont pas gard�es
	*/

	system("pause");
	return 0;
}